<?php

use App\Libraries\Globals;
use App\Models\AppAdmin;
use App\Models\AppWebsiteContent;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        try {
            DB::beginTransaction();

            $this->seedAppAdmins();
            $this->seedAppWebsiteContents();

            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();

            Globals::logException($e);
        }
    }

    private function seedAppAdmins()
    {
        AppAdmin::create([
            'username' => 'sysadmin',
            'password' => Hash::make('qwe123'),
            'email' => 'sysadmin@em.co',
            'status_code' => AppAdmin::STATUS_CODE_ACTIVE,
        ]);

        AppAdmin::create([
            'username' => 'admin123',
            'password' => Hash::make('qwe123'),
            'email' => 'admin123@em.co',
            'status_code' => AppAdmin::STATUS_CODE_ACTIVE,
        ]);
    }

    private function seedAppWebsiteContents()
    {
        AppWebsiteContent::create([
            'key' => 'pageTitle',
            'format' => AppWebsiteContent::FORMAT_TEXT,
            'value' => 'StartBootstrap',
        ]);

        AppWebsiteContent::create([
            'key' => 'headerLeadIn',
            'format' => AppWebsiteContent::FORMAT_TEXT,
            'value' => 'Welcome To Our Studio!',
        ]);

        AppWebsiteContent::create([
            'key' => 'headerHeading',
            'format' => AppWebsiteContent::FORMAT_TEXT,
            'value' => 'It\'s Nice To Meet You',
        ]);

        AppWebsiteContent::create([
            'key' => 'copyright',
            'format' => AppWebsiteContent::FORMAT_TEXT,
            'value' => 'Copyright © StartBootstrap 2019',
        ]);

        AppWebsiteContent::create([
            'key' => 'twitterUrl',
            'format' => AppWebsiteContent::FORMAT_TEXT,
            'value' => '',
        ]);

        AppWebsiteContent::create([
            'key' => 'facebookUrl',
            'format' => AppWebsiteContent::FORMAT_TEXT,
            'value' => '',
        ]);

        AppWebsiteContent::create([
            'key' => 'linkedInUrl',
            'format' => AppWebsiteContent::FORMAT_TEXT,
            'value' => '',
        ]);

        AppWebsiteContent::create([
            'key' => 'privacyPolicyUrl',
            'format' => AppWebsiteContent::FORMAT_TEXT,
            'value' => '',
        ]);

        AppWebsiteContent::create([
            'key' => 'termsOfUseUrl',
            'format' => AppWebsiteContent::FORMAT_TEXT,
            'value' => '',
        ]);

        AppWebsiteContent::create([
            'key' => 'serviceHeading',
            'format' => AppWebsiteContent::FORMAT_TEXT,
            'value' => 'Lorem ipsum dolor sit amet consectetur.',
        ]);

        AppWebsiteContent::create([
            'key' => 'serviceItems',
            'format' => AppWebsiteContent::FORMAT_JSON,
            'value' => json_encode([
                [
                    'icon' => 'fa-shopping-cart',
                    'title' => 'E-Commerce',
                    'content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.',
                ],
                [
                    'icon' => 'fa-laptop',
                    'title' => 'Responsive Design',
                    'content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.',
                ],
                [
                    'icon' => 'fa-lock',
                    'title' => 'Web Security',
                    'content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.',
                ],
            ]),
        ]);

        AppWebsiteContent::create([
            'key' => 'portfolioHeading',
            'format' => AppWebsiteContent::FORMAT_TEXT,
            'value' => 'Lorem ipsum dolor sit amet consectetur.',
        ]);

        AppWebsiteContent::create([
            'key' => 'portfolioItems',
            'format' => AppWebsiteContent::FORMAT_JSON,
            'value' => json_encode([
                [
                    'imageUrl' => asset('/templates/startbootstrap/img/portfolio/01-thumbnail.jpg'),
                    'title' => 'Threads',
                    'content' => 'Illustration',
                ],
                [
                    'imageUrl' => asset('/templates/startbootstrap/img/portfolio/02-thumbnail.jpg'),
                    'title' => 'Explore',
                    'content' => 'Graphic Design',
                ],
                [
                    'imageUrl' => asset('/templates/startbootstrap/img/portfolio/03-thumbnail.jpg'),
                    'title' => 'Finish',
                    'content' => 'Identity',
                ],
                [
                    'imageUrl' => asset('/templates/startbootstrap/img/portfolio/04-thumbnail.jpg'),
                    'title' => 'Lines',
                    'content' => 'Branding',
                ],
                [
                    'imageUrl' => asset('/templates/startbootstrap/img/portfolio/05-thumbnail.jpg'),
                    'title' => 'Southwest',
                    'content' => 'Website Design',
                ],
                [
                    'imageUrl' => asset('/templates/startbootstrap/img/portfolio/06-thumbnail.jpg'),
                    'title' => 'Window',
                    'content' => 'Photography',
                ],
            ]),
        ]);

        AppWebsiteContent::create([
            'key' => 'aboutHeading',
            'format' => AppWebsiteContent::FORMAT_TEXT,
            'value' => 'Lorem ipsum dolor sit amet consectetur.',
        ]);

        AppWebsiteContent::create([
            'key' => 'aboutItems',
            'format' => AppWebsiteContent::FORMAT_JSON,
            'value' => json_encode([
                [
                    'time' => '2009-2011',
                    'title' => 'Our Humble Beginnings',
                    'content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!',
                    'imageUrl' => asset('/templates/startbootstrap/img/about/1.jpg'),
                    'direction' => 'left',
                ],
                [
                    'time' => 'March 2011',
                    'title' => 'An Agency is Born',
                    'content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!',
                    'imageUrl' => asset('/templates/startbootstrap/img/about/2.jpg'),
                    'direction' => 'right',
                ],
                [
                    'time' => 'December 2012',
                    'title' => 'Transition to Full Service',
                    'content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!',
                    'imageUrl' => asset('/templates/startbootstrap/img/about/3.jpg'),
                    'direction' => 'left',
                ],
                [
                    'time' => 'July 2014',
                    'title' => 'Phase Two Expansion',
                    'content' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!',
                    'imageUrl' => asset('/templates/startbootstrap/img/about/4.jpg'),
                    'direction' => 'right',
                ],
            ]),
        ]);

        AppWebsiteContent::create([
            'key' => 'teamHeading',
            'format' => AppWebsiteContent::FORMAT_TEXT,
            'value' => 'Lorem ipsum dolor sit amet consectetur.',
        ]);

        AppWebsiteContent::create([
            'key' => 'teamItems',
            'format' => AppWebsiteContent::FORMAT_JSON,
            'value' => json_encode([
                [
                    'imageUrl' => asset('/templates/startbootstrap/img/team/1.jpg'),
                    'name' => 'Kay Garland',
                    'designation' => 'Lead Designer',
                    'twitterUrl' => '',
                    'facebookUrl' => '',
                    'linkedInUrl' => '',
                ],
                [
                    'imageUrl' => asset('/templates/startbootstrap/img/team/2.jpg'),
                    'name' => 'Larry Parker',
                    'designation' => 'Lead Marketer',
                    'twitterUrl' => '',
                    'facebookUrl' => '',
                    'linkedInUrl' => '',
                ],
                [
                    'imageUrl' => asset('/templates/startbootstrap/img/team/3.jpg'),
                    'name' => 'Diana Pertersen',
                    'designation' => 'Lead Developer',
                    'twitterUrl' => '',
                    'facebookUrl' => '',
                    'linkedInUrl' => '',
                ],
            ]),
        ]);

        AppWebsiteContent::create([
            'key' => 'teamIntro',
            'format' => AppWebsiteContent::FORMAT_TEXT,
            'value' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut eaque, laboriosam veritatis, quos non quis ad perspiciatis, totam corporis ea, alias ut unde.',
        ]);

        AppWebsiteContent::create([
            'key' => 'clientItems',
            'format' => AppWebsiteContent::FORMAT_JSON,
            'value' => json_encode([
                [
                    'imageUrl' => asset('/templates/startbootstrap/img/logos/envato.jpg'),
                ],
                [
                    'imageUrl' => asset('/templates/startbootstrap/img/logos/designmodo.jpg'),
                ],
                [
                    'imageUrl' => asset('/templates/startbootstrap/img/logos/themeforest.jpg'),
                ],
                [
                    'imageUrl' => asset('/templates/startbootstrap/img/logos/creative-market.jpg'),
                ],
            ]),
        ]);

        AppWebsiteContent::create([
            'key' => 'contactUsHeading',
            'format' => AppWebsiteContent::FORMAT_TEXT,
            'value' => 'Lorem ipsum dolor sit amet consectetur.',
        ]);
    }
}
