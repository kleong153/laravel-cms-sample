<script type="text/javascript" src="{{ asset('/js/bootbox-v5/bootbox.js') }}"></script>
<script type="text/javascript">
    function swLoading(msg) {
        swCloseAll();

        bootbox.dialog({
            closeButton: false,
            title: "{{ Lang::get('m.please_wait') }}",
            message: '<div class="modal-content-holder"><img alt="" src="{{ asset('/images/loading.gif') }}" class="img-loading" style="height: auto; width: 40px;" />&nbsp;&nbsp;&nbsp;' + swFormatMessage(msg, "{{ Lang::get('m.loading') }}...") + '</div>'
        });
    }

    function swAlert(msg, fn) {
        if (fn === undefined || fn == null) {
            fn = function() {};
        }

        swCloseAll();

        bootbox.dialog({
            title: '<span><i class="fa fa-exclamation-circle"></i> {{ Lang::get('m.message') }}</span>',
            message: '<div class="modal-content-holder text-left">' + swFormatMessage(msg) + '</div>',
            closeButton: fn,
            backdrop: fn,
            onEscape: fn,
            buttons: {
                confirm: {
                    label: "{{ Lang::get('m.ok') }}",
                    className: "btn-primary",
                    callback: fn
                }
            }
        });
    }

    function swSuccess(msg, fn) {
        if (fn === undefined || fn == null) {
            fn = function() {};
        }

        swCloseAll();

        bootbox.dialog({
            title: '<span style="color: #198f71;"><i class="fa fa-check-circle"></i> {{ Lang::get('m.success') }}</span>',
            message: '<div class="modal-content-holder text-left"><span style="color: #198f71;">' + swFormatMessage(msg) + '</span></div>',
            closeButton: fn,
            backdrop: fn,
            onEscape: fn,
            buttons: {
                confirm: {
                    label: "{{ Lang::get('m.ok') }}",
                    className: "btn-primary",
                    callback: fn
                }
            }
        });
    }

    function swError(msg, fn) {
        if (fn === undefined || fn == null) {
            fn = function() {};
        }

        swCloseAll();

        bootbox.dialog({
            title: '<span style="color: #c9302c;"><i class="fa fa-times-circle"></i> {{ Lang::get('m.error') }}</span>',
            message: '<div class="modal-content-holder text-left"><span style="color: #c9302c;">' + swFormatMessage(msg) + '</span></div>',
            closeButton: fn,
            backdrop: fn,
            onEscape: fn,
            buttons: {
                confirm: {
                    label: "{{ Lang::get('m.ok') }}",
                    className: "btn-primary",
                    callback: fn
                }
            }
        });
    }

    function swConfirm(msg, fnConfirm, fnCancel) {
        if (fnCancel === undefined || fnCancel == null) {
            fnCancel = function() {};
        }

        swCloseAll();

        bootbox.dialog({
            title: '<span><i class="fa fa-question-circle"></i> {{ Lang::get('m.question') }}</span>',
            message: '<div class="modal-content-holder text-left">' + swFormatMessage(msg) + '</div>',
            closeButton: fnCancel,
            backdrop: fnCancel,
            onEscape: fnCancel,
            buttons: {
                confirm: {
                    label: "{{ Lang::get('m.ok') }}",
                    className: "btn-success",
                    callback: fnConfirm
                },
                cancel: {
                    label: "{{ Lang::get('m.cancel') }}",
                    className: "btn-danger",
                    callback: fnCancel
                }
            }
        });
    }

    function swFormatMessage(msg, ifNull) {
        var msgs = "";

        if ($.isArray(msg)) {
            $.each(msg, function (key, value) {
                msgs = value + "<br>";
            });
        } else if (msg === undefined || msg == null) {
            msgs = swFormatMessage(ifNull, "");
        } else {
            msgs = msg;
        }

        return msgs;
    }

    function swCloseAll() {
        bootbox.hideAll();
    }

    function swShowCustomModal(modalSelector, show) {
        if (show) {
            setTimeout(function () {
                modalSelector.modal("show");
            }, 300);
        } else {
            modalSelector.modal("hide");
        }
    }
</script>