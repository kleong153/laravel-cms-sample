@section('js')
@parent

<script type="text/javascript" src="{{ asset('/vendor/jsvalidation/js/jsvalidation.js') }}"></script>

{!! $jsValidator !!}

@endsection