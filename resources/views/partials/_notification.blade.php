@if (Session::has(Globals::MSG_INFO))
<div class="alert alert-info alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong><i class="icon fa fa-info"></i></strong> {!! Session::get(Globals::MSG_INFO) !!}
</div>
@endif

@if (Session::has(Globals::MSG_SUCCESS))
<div class="alert alert-success alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong><i class="icon fa fa-check"></i></strong> {!! Session::get(Globals::MSG_SUCCESS) !!}
</div>
@endif

@if (Session::has(Globals::MSG_ERROR))
<div class="alert alert-danger alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong><i class="icon fa fa-ban"></i></strong> {!! Session::get(Globals::MSG_ERROR) !!}
</div>
@endif

@if (isset($errors) && count($errors) > 0)
<div class="alert alert-danger alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <strong><i class="icon fa fa-ban"></i></strong> {!! Lang::get('m.problems_with_inputs') !!}
    <br><br>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>

<input type="hidden" id="validator_error_keys" value="{{ implode('|', $errors->keys()) }}">
@endif