@section('css-top')
@parent

<link rel="stylesheet" href="{{ asset('/js/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}">
@endsection

@section('js-top')
@parent

<script type="text/javascript" src="{{ asset('/js/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
@endsection