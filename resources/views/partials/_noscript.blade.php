<noscript>
    <!-- display message if java is turned off -->
    <div style="width: 100%; background-color: #a94442; color: #fff; text-align: center; padding: 3px 0px;">
        {{ Lang::get('m.noscript_warning') }}
    </div>
</noscript>