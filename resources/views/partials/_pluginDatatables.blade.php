@section('css-top')
@parent

<link rel="stylesheet" href="{{ asset('/js/datatables/bootstrap4/css/dataTables.bootstrap4.min.css') }}">
@endsection

@section('js-top')
@parent

<script type="text/javascript" src="{{ asset('/js/datatables/bootstrap4/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/js/datatables/bootstrap4/js/dataTables.bootstrap4.min.js') }}"></script>
@endsection