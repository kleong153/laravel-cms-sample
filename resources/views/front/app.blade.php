<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>{{ $data->get('pageTitle') }}</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('/templates/startbootstrap/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="{{ asset('/templates/startbootstrap/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="{{ asset('/templates/startbootstrap/css/agency.min.css') }}" rel="stylesheet">

    @yield('css-top')

    <link rel="stylesheet" href="{{ asset('/css/project_helper.css') }}" />

    @yield('css')

</head>

<body id="page-top">

    @include('partials._noscript')

    @yield('content')

    <!-- Bootstrap core JavaScript -->
    <script src="{{ asset('/templates/startbootstrap/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('/templates/startbootstrap/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <!-- Plugin JavaScript -->
    <script src="{{ asset('/templates/startbootstrap/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

    <!-- Contact form JavaScript -->
    <script src="{{ asset('/templates/startbootstrap/js/jqBootstrapValidation.js') }}"></script>
    <script src="{{ asset('/templates/startbootstrap/js/contact_me.js') }}"></script>

    <!-- Custom scripts for this template -->
    <script src="{{ asset('/templates/startbootstrap/js/agency.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/js/jquery/jquery-validator/jquery.validate.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery/jquery-validator/additional-methods.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/project_helper.js') }}"></script>

    @yield('js-top')

    @include('partials._pluginSwDialog')

    @yield('js')

    @yield('modal')

</body>

</html>