@extends('front.app')

@include('partials._pluginJsValidator')

@section('js')
@parent

<script type="text/javascript">
    function customFormSubmitHandler(form) {
        var btnObj = $("#submit_btn");

        if (btnObj.prop("disabled")) {
            return false;
        }

        swConfirm("{{ Lang::get('m.confirm_submit_this_message') }}", function () {
            btnObj.prop("disabled", true).text("{{ Lang::get('m.loading') }}");

            form.submit();
        });
    }
</script>
@endsection

@section('content')
<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">{{ $data->get('pageTitle') }}</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            Menu
            <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav text-uppercase ml-auto">
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#services">Services</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#portfolio">Portfolio</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#about">About</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#team">Team</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<!-- Header -->
<header class="masthead">
    <div class="container">
        <div class="intro-text">
            <div class="intro-lead-in">{{ $data->get('headerLeadIn') }}</div>
            <div class="intro-heading text-uppercase">{{ $data->get('headerHeading') }}</div>
            <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="#services">Tell Me More</a>
        </div>
    </div>
</header>

<!-- Services -->
<section class="page-section" id="services">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading text-uppercase">Services</h2>
                <h3 class="section-subheading text-muted">{{ $data->get('serviceHeading') }}</h3>
            </div>
        </div>
        <div class="row text-center">
            @foreach ($data->get('serviceItems') as $item)
            <div class="col-md-4">
                <span class="fa-stack fa-4x">
                    <i class="fas fa-circle fa-stack-2x text-primary"></i>
                    <i class="fas {{ $item['icon'] }} fa-stack-1x fa-inverse"></i>
                </span>
                <h4 class="service-heading">{{ $item['title'] }}</h4>
                <p class="text-muted">{{ $item['content'] }}</p>
            </div>
            @endforeach
        </div>
    </div>
</section>

<!-- Portfolio Grid -->
<section class="bg-light page-section" id="portfolio">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading text-uppercase">Portfolio</h2>
                <h3 class="section-subheading text-muted">{{ $data->get('portfolioHeading') }}</h3>
            </div>
        </div>
        <div class="row">
            @foreach ($data->get('portfolioItems') as $item)
            <div class="col-md-4 col-sm-6 portfolio-item">
                <img class="img-fluid" src="{{ $item['imageUrl'] }}" alt="">
                <div class="portfolio-caption">
                    <h4>{{ $item['title'] }}</h4>
                    <p class="text-muted">{{ $item['content'] }}</p>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>

<!-- About -->
<section class="page-section" id="about">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading text-uppercase">About</h2>
                <h3 class="section-subheading text-muted">{{ $data->get('aboutHeading') }}</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <ul class="timeline">
                    @foreach ($data->get('aboutItems') as $item)
                    <li class="{{ $item['direction'] == 'right' ? 'timeline-inverted' : '' }}">
                        <div class=" timeline-image">
                            <img class="rounded-circle img-fluid" src="{{ $item['imageUrl'] }}" alt="">
                        </div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4>{{ $item['time'] }}</h4>
                                <h4 class="subheading">{{ $item['title'] }}</h4>
                            </div>
                            <div class="timeline-body">
                                <p class="text-muted">{{ $item['content'] }}</p>
                            </div>
                        </div>
                    </li>
                    @endforeach
                    <li class="timeline-inverted">
                        <div class="timeline-image">
                            <h4>Be Part
                                <br>Of Our
                                <br>Story!</h4>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>

<!-- Team -->
<section class="bg-light page-section" id="team">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading text-uppercase">Our Amazing Team</h2>
                <h3 class="section-subheading text-muted">{{ $data->get('teamHeading') }}</h3>
            </div>
        </div>
        <div class="row">
            @foreach ($data->get('teamItems') as $item)
            <div class="col-sm-4">
                <div class="team-member">
                    <img class="mx-auto rounded-circle" src="{{ $item['imageUrl'] }}" alt="">
                    <h4>{{ $item['name'] }}</h4>
                    <p class="text-muted">{{ $item['designation'] }}</p>
                    <ul class="list-inline social-buttons">
                        <li class="list-inline-item">
                            <a href="{{ $item['twitterUrl'] }}">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="{{ $item['facebookUrl'] }}">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                        </li>
                        <li class="list-inline-item">
                            <a href="{{ $item['linkedInUrl'] }}">
                                <i class="fab fa-linkedin-in"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            @endforeach
        </div>
        <div class="row">
            <div class="col-lg-8 mx-auto text-center">
                <p class="large text-muted">{{ $data->get('teamIntro') }}</p>
            </div>
        </div>
    </div>
</section>

<!-- Clients -->
<section class="py-5">
    <div class="container">
        <div class="row">
            @foreach ($data->get('clientItems') as $item)
            <div class="col-md-3 col-sm-6">
                <img class="img-fluid d-block mx-auto" src="{{ $item['imageUrl'] }}" alt="">
            </div>
            @endforeach
        </div>
    </div>
</section>

<!-- Contact -->
<section class="page-section" id="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading text-uppercase">Contact Us</h2>
                <h3 class="section-subheading text-muted">{{ $data->get('contactUsHeading') }}</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">

                @include('partials._notification')

                <form id="main_form" action="{{ $currentUrl }}" method="post">

                    <input type="hidden" id="_act" name="_act" value="do_save">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <input class="form-control" id="name" name="name" type="text" placeholder="Your Name *">
                            </div>
                            <div class="form-group">
                                <input class="form-control" id="email" name="email" type="email" placeholder="Your Email *">
                            </div>
                            <div class="form-group">
                                <input class="form-control" id="contact_no" name="contact_no" type="tel" placeholder="Your Phone *">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <textarea class="form-control" id="message" name="message" placeholder="Your Message *"></textarea>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-12 text-center">
                            <div id="success"></div>
                            <button id="submit_btn" class="btn btn-primary btn-xl text-uppercase" type="submit">Send Message</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<!-- Footer -->
<footer class="footer">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-4">
                <span class="copyright">{{ $data->get('copyright') }}</span>
            </div>
            <div class="col-md-4">
                <ul class="list-inline social-buttons">
                    <li class="list-inline-item">
                        <a href="{{ $data->get('twitterUrl') }}">
                            <i class="fab fa-twitter"></i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="{{ $data->get('facebookUrl') }}">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a href="{{ $data->get('linkedInUrl') }}">
                            <i class="fab fa-linkedin-in"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-4">
                <ul class="list-inline quicklinks">
                    <li class="list-inline-item">
                        <a href="{{ $data->get('privacyPolicyUrl') }}">Privacy Policy</a>
                    </li>
                    <li class="list-inline-item">
                        <a href="{{ $data->get('privacyPolicyUrl') }}">Terms of Use</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
@endsection