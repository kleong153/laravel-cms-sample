@extends('admin.appAuth')

@section('title', Globals::formatPageTitle('m.sign_in', true))

@section('js')
@parent

<script type="text/javascript">
    $(function () {
        $("body").addClass("login-page");

        initCaptchaImg($(".captcha-img"), $("#captcha_img_refresh"));

        retainInputIChecker({
            "remember": "{{ old("remember") }}"
        });

        $("#main_form").validate(validatorOptWithStyle({
            rules: {
                "username": {
                    required: true
                },
                "password": {
                    required: true
                },
                "captcha": {
                    required: true
                }
            },
            errorPlacement: function (error, element) {
                error.insertBefore(element.parent("div"));

                if (element.attr("name") == "captcha") {
                    error.insertBefore($(element.parent("div")).parent("div"));
                }
            },
            submitHandler: function (form) {
                var btnObj = $("#submit_btn");

                if (btnObj.prop("disabled")) {
                    return false;
                }

                btnObj.prop("disabled", true).text("{{ Lang::get('m.loading') }}");

                form.submit();
            }
        }));
    });
</script>
@endsection

@section('content')
<div class="login-box">
    <div class="login-logo">
        <a href="{{ route('admin@login') }}"><b>{{ Lang::get('m._project_name') }}</b></a>
    </div>

    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">{{ Lang::get('m.sign_in') }}</p>

            <form id="main_form" action="{{ route('admin@login') }}" method="post">

                @include('partials._notification')

                <input type="hidden" id="user_role" name="user_role" value="admin" />

                <div class="input-group mb-3">
                    <input type="text" class="form-control" id="username" name="username" placeholder="{{ Lang::get('m.username') }}" value="{{ old('username') }}" />
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-envelope"></span>
                        </div>
                    </div>
                </div>

                <div class="input-group mb-3">
                    <input type="password" class="form-control" id="password" name="password" placeholder="{{ Lang::get('m.password') }}" />
                    <div class="input-group-append">
                        <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-12 col-md-5">
                        <p><img src="{{ captcha_src() }}" alt="{{ Lang::get('m.captcha') }}" class="captcha-img click-able" title="{{ Lang::get('m.click_to_refresh') }}" data-refresh-config="default"></p>
                    </div>

                    <div class="col-12 col-md-7">
                        <input type="text" id="captcha" name="captcha" class="form-control form-control-lg" placeholder="{{ Lang::get('m.enter_captcha') }}">
                    </div>

                    <div class="col-12 text-left">
                        <a id="captcha_img_refresh" href="javascript:void(0);">{{ Lang::get('m.refresh_captcha') }}</a>
                    </div>
                </div>

                <div class="row">
                    <div class="col-8">
                        <div class="icheck-primary">
                            <input type="checkbox" id="remember" name="remember" />
                            <label for="remember">{{ Lang::get('m.remember_me') }}</label>
                        </div>
                    </div>

                    <div class="col-4">
                        <button type="submit" class="btn btn-primary btn-block" id="submit_btn">{{ Lang::get('m.sign_in') }}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection