@extends('admin.app')

@include('partials._pluginJsValidator')

@section('title', Globals::formatPageTitle($pageTitle, true))
@section('header', $pageTitle)
@section('sidebar-select', $backUrl)

@section('js')
@parent

<script type="text/javascript">
    $(function () {
        retainInputs({
            "status_code": "{{ $data->get('status_code') }}"
        });
    });

    function customFormSubmitHandler(form) {
        var btnObj = $("#submit_btn");

        if (btnObj.prop("disabled")) {
            return false;
        }

        swConfirm("{{ Lang::get('m.confirm_save_this_record') }}", function () {
            btnObj.prop("disabled", true).text("{{ Lang::get('m.loading') }}");

            form.submit();
        });
    }
</script>
@endsection

@section('content')
<div class="container-fluid">

    @include('partials._notification')

    <div class="row">
        <div class="col-12">
            <div class="card card-primary card-outline">
                <div class="card-header">
                    <h5 class="m-0">{{ Lang::get('m.contact_us_details') }}</h5>
                </div>

                <form id="main_form" class="form-horizontal" action="{{ $currentUrl }}" method="post">

                    <input type="hidden" name="_act" value="do_save">
                    <input type="hidden" id="id" name="id" value="{{ $data->get('id') }}">

                    <div class="card-body">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">{{ $attrNames['name'] }}</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" value="{{ $data->get('name') }}" disabled readonly />
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">{{ $attrNames['email'] }}</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" value="{{ $data->get('email') }}" disabled readonly />
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">{{ $attrNames['contact_no'] }}</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" value="{{ $data->get('contact_no') }}" disabled readonly />
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">{{ $attrNames['message'] }}</label>
                            <div class="col-sm-7">
                                <textarea class="form-control" rows="5" disabled readonly>{{ $data->get('message') }}</textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">{{ $attrNames['status_code'] }}</label>
                            <div class="col-sm-7">
                                <select class="form-control" id="status_code" name="status_code">
                                    @foreach ($statusCodeOptions as $key => $val)
                                    <option value="{{ $key }}">{{ $val }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">{{ $attrNames['created_at'] }}</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" value="{{ $data->get('created_at') }}" disabled readonly />
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">{{ $attrNames['updated_at'] }}</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" value="{{ $data->get('updated_at') }}" disabled readonly />
                            </div>
                        </div>
                    </div>

                    <div class="card-footer">
                        <a href="{{ $backUrl }}" class="btn btn-default"><i class="fa fa-arrow-circle-left mr-2"></i> {{ Lang::get('m.back') }}</a>

                        <button type="submit" class="btn btn-primary float-right" id="submit_btn">{{ Lang::get('m.save') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection