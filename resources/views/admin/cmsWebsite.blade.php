@extends('admin.app')

@include('partials._pluginJsValidator')

@section('title', Globals::formatPageTitle($pageTitle, true))
@section('header', $pageTitle)

@section('js')
@parent

<script type="text/javascript">
    function customFormSubmitHandler(form) {
        var btnObj = $("#submit_btn");

        if (btnObj.prop("disabled")) {
            return false;
        }

        swConfirm("{{ Lang::get('m.confirm_save_this_record') }}", function () {
            btnObj.prop("disabled", true).text("{{ Lang::get('m.loading') }}");

            form.submit();
        });
    }
</script>
@endsection

@section('content')
<div class="container-fluid">

    @include('partials._notification')

    <div class="row">
        <div class="col-12">
            <div class="card card-primary card-outline">
                <div class="card-header">
                    <h5 class="m-0">{{ Lang::get('m.website_content') }}</h5>
                </div>

                <form id="main_form" class="form-horizontal" action="{{ $currentUrl }}" method="post">

                    <input type="hidden" name="_act" value="do_save">

                    <div class="card-body">
                        @foreach ($data as $key => $val)
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">{{ $attrNames[$key] }}</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="{{ $key }}" name="{{ $key }}" value="{{ old($key, $val) }}" />
                            </div>
                        </div>
                        @endforeach
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary float-right" id="submit_btn">{{ Lang::get('m.save') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection