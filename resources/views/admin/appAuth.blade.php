<!DOCTYPE html>
<html lang="{{ Lang::getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', Lang::get('m._project_name'))</title>

    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('/templates/adminlte/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{ asset('/templates/adminlte/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('/templates/adminlte/dist/css/adminlte.min.css') }}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    @yield('css-top')

    <link rel="stylesheet" href="{{ asset('/css/project_helper.css') }}" />

    @yield('css')
</head>

<body class="hold-transition">

    @include('partials._noscript')

    @yield('content')

    <!-- jQuery -->
    <script src="{{ asset('/templates/adminlte/plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('/templates/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('/templates/adminlte/dist/js/adminlte.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/js/jquery/jquery-validator/jquery.validate.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/jquery/jquery-validator/additional-methods.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/project_helper.js') }}"></script>

    @yield('js-top')

    @include('partials._pluginSwDialog')

    @yield('js')

    @yield('modal')

</body>

</html>