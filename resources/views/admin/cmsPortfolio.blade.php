@extends('admin.app')

@include('partials._pluginJsValidator')

@section('title', Globals::formatPageTitle($pageTitle, true))
@section('header', $pageTitle)

@section('js')
@parent

<script src="{{ asset('templates/adminlte/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
<script type="text/javascript">
    $(function () {
        bsCustomFileInput.init();
    });

    function customFormSubmitHandler(form) {
        var btnObj = $("#submit_btn");

        if (btnObj.prop("disabled")) {
            return false;
        }

        swConfirm("{{ Lang::get('m.confirm_save_this_record') }}", function () {
            btnObj.prop("disabled", true).text("{{ Lang::get('m.loading') }}");

            form.submit();
        });
    }
</script>
@endsection

@section('content')
<div class="container-fluid">

    @include('partials._notification')

    <div class="row">
        <div class="col-12">
            <div class="card card-primary card-outline">
                <div class="card-header">
                    <h5 class="m-0">{{ Lang::get('m.portfolio_content') }}</h5>
                </div>

                <form id="main_form" class="form-horizontal" action="{{ $currentUrl }}" method="post" enctype="multipart/form-data">

                    <input type="hidden" name="_act" value="do_save">

                    <div class="card-body">
                        @foreach ($data as $key => $val)
                        @if ($key == 'portfolio_items')
                        {{-- Use special template. --}}
                        @foreach ($val as $vindex => $varray)
                        <hr />
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">{{ $attrNames[$key] }} {{ $vindex + 1 }}</label>
                        </div>
                        @foreach ($varray as $vk => $vv)
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">{{ $attrNames[$vk] }}</label>
                            <div class="col-sm-7">
                                @if ($vk == 'imageUrl')
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="{{ $vk . '_' . $vindex }}" name="{{ $vk . '_' . $vindex }}" />
                                        <label for="{{ $vk . '_' . $vindex }}" class="custom-file-label">{{ Lang::get('m.choose_file') }}</label>
                                    </div>
                                </div>
                                <small class="form-text text-muted">{{ Lang::get('m.image_size_x', ['p1' => '400px x 300px']) }}</small>
                                <img src="{{ $vv }}" alt="" class="img-fluid mt-3" />
                                @elseif ($vk == 'content')
                                <textarea class="form-control" id="{{ $vk . '_' . $vindex }}" name="{{ $vk . '_' . $vindex }}" rows="3">{{ old($vk . '_' . $vindex, $vv) }}</textarea>
                                @else
                                <input type="text" class="form-control" id="{{ $vk . '_' . $vindex }}" name="{{ $vk . '_' . $vindex }}" value="{{ old($vk . '_' . $vindex, $vv) }}" />
                                @endif
                            </div>
                        </div>
                        @endforeach
                        @endforeach
                        @else
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">{{ $attrNames[$key] }}</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="{{ $key }}" name="{{ $key }}" value="{{ old($key, $val) }}" />
                            </div>
                        </div>
                        @endif
                        @endforeach
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary float-right" id="submit_btn">{{ Lang::get('m.save') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection