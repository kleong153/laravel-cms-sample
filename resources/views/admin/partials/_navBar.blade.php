<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('admin@home') }}" class="brand-link">
        <img src="{{ asset('/templates/adminlte/dist/img/AdminLTELogo.png') }}" alt="Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">{{ Lang::get('m._project_name') }}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="{{ route('admin@home') }}" class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>{{ Lang::get('m.dashboard') }}</p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link nav-treeview-header">
                        <i class="nav-icon fas fa-user"></i>
                        <p>
                            {{ Lang::get('m.my_profile') }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('admin@profile_settings') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ Lang::get('m.profile_settings') }}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('admin@password_settings') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ Lang::get('m.password_settings') }}</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link nav-treeview-header">
                        <i class="nav-icon fas fa-pen"></i>
                        <p>
                            {{ Lang::get('m.site') }}
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('admin@cms_website') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ Lang::get('m.website') }}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('admin@cms_services') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ Lang::get('m.services') }}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('admin@cms_portfolio') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ Lang::get('m.portfolio') }}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('admin@cms_about') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ Lang::get('m.about') }}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('admin@cms_our_team') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ Lang::get('m.our_team') }}</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('admin@cms_our_clients') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>{{ Lang::get('m.our_clients') }}</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin@contact_us_list') }}" class="nav-link">
                        <i class="nav-icon fas fa-envelope"></i>
                        <p>{{ Lang::get('m.contact_us') }}</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('admin@logout') }}" class="nav-link">
                        <i class="nav-icon fas fa-sign-out-alt"></i>
                        <p>{{ Lang::get('m.sign_out') }}</p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>