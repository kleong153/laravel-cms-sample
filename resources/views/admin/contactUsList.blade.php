@extends('admin.app')

@include('partials._pluginDatatables')
@include('partials._pluginDatePicker')

@section('title', Globals::formatPageTitle($pageTitle))
@section('header', $pageTitle)

@section('js')
@parent

<script type="text/javascript">
    var jDatatable = null;

        $(function () {
            jDatatable = $("#jq_datatable").DataTable({
                "autoWidth": false,
                "cache": false,
                "lengthChange": true,
                "processing": true,
                "searching": false,
                "serverSide": true,
                "pagingType": "full_numbers",
                "pageLength": 50,
                "lengthMenu": [50, 100, 250, 500],
                "language": {
                    "url": "{{ Globals::getDataTablesLangUrl() }}"
                },
                "ajax": {
                    "url": $("#search_form").attr("action"),
                    "type": "POST",
                    "cache": false,
                    "data": function (d) {
                        return getDtAjaxData(d);
                    },
                    "error": function (XMLHttpRequest, textStatus, errorThrown) {
                        swError("{{ Lang::get('m.request_outdated_please_retry') }}");
                    }
                },
                "order": [[0, "desc"]],
                "columnDefs": [
                    {
                        "targets": [],
                        "sortable": false
                    },
                    {
                        "targets": [0, 1, 2, 3, 4],
                        "className": "dt-body-center"
                    }
                ],
                "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    $("td:eq(1)", nRow).html('<a href="{{ route('admin@contact_us_details') }}?id=' + aData[5] + '">' + aData[1] + '</a>');
                }
            });

            initDatePicker($(".date-picker"), "{{ Lang::getLocale() }}");

            initDtSearchButton($("#search_btn"), $("#search_form"), jDatatable);

            initDtExportExcelButton($("#export_excel_btn"), $("#search_form"), jDatatable);
        });

        function getDtAjaxData(data) {
            data._act = "do_get_list";
            data.searchParams = getDtSearchParams($("#search_form"));

            return data;
        }
</script>
@endsection

@section('content')
<div class="container-fluid">

    @include('partials._notification')

    <div class="row">
        <div class="col-12">
            <div class="card card-primary card-outline">
                <div class="card-body">
                    <form id="search_form" class="form-horizontal" action="{{ $currentUrl }}" method="post">
                        <div class="row">
                            <div class="col-md-6 g-mb-20">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">{{ $attrNames['name'] }}</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" id="name" name="name" />
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 g-mb-20">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">{{ $attrNames['email'] }}</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" id="email" name="email" />
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 g-mb-20">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">{{ $attrNames['contact_no'] }}</label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control" id="contact_no" name="contact_no" />
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 g-mb-20">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">{{ $attrNames['status_code'] }}</label>
                                    <div class="col-sm-7">
                                        <select class="form-control" id="status_code" name="status_code">
                                            @foreach (App\Models\AppContactUsSubmission::getStatusCodeOptions(true) as $key => $val)
                                            <option value="{{ $key }}">{{ $val }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 g-mb-20">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">{{ $attrNames['created_at'] }}</label>
                                    <div class="col-sm-7">
                                        <div class="input-daterange input-group date-picker">
                                            <input type="text" class="form-control" name="created_at_start">
                                            <div class="input-group-append input-group-prepend">
                                                <span class="input-group-text">{{ Lang::get('m.to') }}</span>
                                            </div>
                                            <input type="text" class="form-control" name="created_at_end">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row mt-3">
                            <div class="col-12">
                                <button type="button" class="btn btn-success float-right" id="search_btn"><i class="fa fa-search mr-2"></i> {{ Lang::get('m.search') }}</button>

                                <button type="button" class="btn btn-default float-right mr-3" id="export_excel_btn"><i class="fa fa-save mr-2"></i> {{ Lang::get('m.export') }}</button>
                            </div>
                        </div>
                    </form>

                    <hr />

                    <table id="jq_datatable" class="table table-bordered table-striped table-hover table-auto-tfoot-colspan" cellspacing="0">
                        <thead>
                            <tr>
                                <th>{{ $attrNames['created_at'] }}</th>
                                <th>{{ $attrNames['name'] }}</th>
                                <th>{{ $attrNames['email'] }}</th>
                                <th>{{ $attrNames['contact_no'] }}</th>
                                <th>{{ $attrNames['status_code'] }}</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection