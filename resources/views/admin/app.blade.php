<!DOCTYPE html>
<html lang="{{ Lang::getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', Lang::get('m._project_name'))</title>

    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{ asset('/templates/adminlte/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('/templates/adminlte/dist/css/adminlte.min.css') }}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    @yield('css-top')

    <link rel="stylesheet" href="{{ asset('/css/project_helper.css') }}" />

    @yield('css')
</head>

<body class="hold-transition sidebar-mini">

    @include('partials._noscript')

    <div class="wrapper">

        @include('admin.partials._header')

        @include('admin.partials._navBar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark">

                                @yield('header')

                            </h1>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <div class="content">

                @yield('content')

            </div>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- Main Footer -->
        <footer class="main-footer">
            <!-- Default to the left -->
            <strong>{{ Lang::get('m.copyright_text', ['p1' => '2019', 'p2' => Lang::get('m._project_name')]) }}</strong>
        </footer>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->

    <!-- jQuery -->
    <script src="{{ asset('/templates/adminlte/plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('/templates/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('/templates/adminlte/dist/js/adminlte.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('/js/project_helper.js') }}"></script>

    @yield('js-top')

    @include('partials._pluginSwDialog')

    <script type="text/javascript">
        $(function () {
            initLeftMenu();
        });

        function initLeftMenu() {
            var url = "@yield("sidebar-select")";

            if (url == "") {
                url = window.location.href;
            }

            url = url.split("?")[0]
                .split("#")[0]
                .match(/(\/[a-zA-z0-9\-/]+)$/g);

            var menu = $("aside.main-sidebar > div.sidebar > nav > ul.nav-sidebar li a[href*=\"" + url + "\"]");
            var menuParent = menu.parent();

            if (menu.length > 0) {
                // Menu found.
                if (menuParent.parent().hasClass("nav-treeview")) {
                    menu.addClass("active");

                    var mpp = menuParent.parent().parent();

                    $("a.nav-treeview-header", mpp).addClass("active");
                    mpp.addClass("menu-open");
                } else {
                    menu.addClass("active");
                }
            }

            // Hide dropdown with empty children.
            $("ul.nav-sidebar .has-treeview").each(function () {
                if ($(".nav-treeview > li", $(this)).length < 1) {
                    $(this).hide();
                }
            });
        }
    </script>

    @yield('js')

    @yield('modal')

</body>

</html>