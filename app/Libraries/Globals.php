<?php

namespace App\Libraries;

abstract class Globals
{
    /************************************/
    /*****      FLASH MESSAGE      ******/
    /************************************/
    const MSG_SUCCESS = 'FLASH_MSG_SUCCESS';
    const MSG_INFO = 'FLASH_MSG_INFO';
    const MSG_ERROR = 'FLASH_MSG_ERROR';

    /************************************/
    /*****         DATETIME        ******/
    /************************************/
    const DATETIME_FORMAT_FULL = 'l j F, Y g:i A';
    const DATETIME_FORMAT_SHORT = 'j F, Y';
    const DATETIME_FORMAT_DATABASE = 'Y-m-d H:i:s';
    const DATETIME_FORMAT_DEFAULT = 'Y-m-d h:i A';
    const DATETIME_FORMAT_DEFAULT2 = 'Y-m-d h:i:s A';
    const DATETIME_FORMAT_YMD = 'Y-m-d';
    const DATETIME_FORMAT_DMY = 'd-m-Y';
    const DATETIME_FORMAT_MDY = 'M d, Y';

    /************************************/
    /*****        CACHE KEY        ******/
    /************************************/
    const CACHE_KEY_WEBSITE_CONTENTS = 'CACHE_KEY_WEBSITE_CONTENTS';

    /************************************/
    /*****   UPLOAD FOLDER PATH    ******/
    /************************************/
    const UPLOAD_PORTFOLIO = "upload_portfolio";
    const UPLOAD_ABOUT = "upload_about";
    const UPLOAD_OUR_TEAM = "upload_our_team";
    const UPLOAD_OUR_CLIENT = "upload_our_client";

    /**
     * @param string $title
     * @param bool $translate
     * @return string
     */
    public static function formatPageTitle($title, $translate = false)
    {
        if ($translate && \Lang::has($title)) {
            return \Lang::get('m._project_name') . ' | ' . \Lang::get($title);
        }

        return \Lang::get('m._project_name') . ' | ' . $title;
    }

    /**
     * @param int $size
     * @return string
     */
    public static function fileSize($size)
    {
        if ($size > 1024 * 1024 * 1024) {
            return number_format(($size / 1024 / 1024 / 1024), 2) . " GB";
        } elseif ($size > 1024 * 1024) {
            return number_format(($size / 1024 / 1024), 2) . " MB";
        } elseif ($size > 1024) {
            return number_format(($size / 1024), 2) . " KB";
        } else {
            return $size . " Bytes";
        }
    }

    /**
     * @return string
     */
    public static function getCaptchaLangCode()
    {
        switch (\Lang::locale()) {
            case "cn":
                return "zh-CN";
            default:
                return config("fallback_locale");
        }
    }

    /**
     * @return string
     */
    public static function getDataTablesLangUrl()
    {
        return url('/js/datatables/localization/' . \Lang::getLocale() . '.json');
    }

    /**
     * @param Illuminate\Support\Facades\Validator $validator
     * @param string $prependMessage
     * @return string
     */
    public static function validatorToMessage($validator, $prependMessage = '')
    {
        $str = '';

        if ($validator->errors()->count() > 0) {

            foreach ($validator->errors()->all() as $m) {
                $str .= '<li>' . $m . '</li>';
            }

            $str = \Lang::get('m.problems_with_inputs') . '<br><br><ul>' . $str . '</ul>';
        }

        if (strlen($prependMessage) > 0) {
            if ($str != '') {
                $str = $prependMessage . '<br><br>' . $str;
            } else {
                $str = $prependMessage;
            }
        }

        return $str;
    }

    /**
     * @param Illuminate\Support\Facades\Validator $validator
     * @param array $invalidOptions
     */
    public static function validatorComposeError($validator, $invalidOptions)
    {
        $count = count($invalidOptions);

        if ($count > 0) {
            // Compose error messages.
            list($keys, $values) = \Arr::divide($invalidOptions);

            $validator->errors()->add(implode('|', $keys), \Lang::choice('m.invalid_option_c', $count) . ': ' . implode(', ', $values));
        }
    }

    /**
     * @param int $length
     * @param string $chars
     * @return string
     */
    public static function random($length, $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz")
    {
        $result = "";
        $max = \Str::length($chars) - 1;

        for ($i = 0; $i < $length; $i++) {
            $result .= $chars[rand(0, $max)];
        }

        return $result;
    }

    /**
     * @param \Exception $exception
     * @param string $type
     */
    public static function logException($exception, $type = 'error')
    {
        $msg = $exception->getMessage() . PHP_EOL . $exception->getTraceAsString();

        if ($type == 'error') {
            \Log::error($msg);
        } else {
            \Log::debug($msg);
        }
    }

    /**
     * @param mixed $value
     * @param string $ifNull
     * @return mixed
     */
    public static function ifNull($value, $ifNull = '')
    {
        return is_null($value) ? $ifNull : $value;
    }

    public static function clearWebsiteContentsCache()
    {
        \Cache::forget(self::CACHE_KEY_WEBSITE_CONTENTS);
    }
}
