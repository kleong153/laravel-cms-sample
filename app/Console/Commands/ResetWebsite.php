<?php

namespace App\Console\Commands;

use App\Libraries\Globals;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class ResetWebsite extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'website:reset';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset website content to original state';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Reset database.
        $this->call('migrate:refresh', ['--seed' => 1]);

        // Delete all files in storage.
        $storage = Storage::disk('public');
        $storage->deleteDirectory(Globals::UPLOAD_ABOUT);
        $storage->deleteDirectory(Globals::UPLOAD_OUR_CLIENT);
        $storage->deleteDirectory(Globals::UPLOAD_OUR_TEAM);
        $storage->deleteDirectory(Globals::UPLOAD_PORTFOLIO);

        $this->comment('Uploaded files deleted.');

        // Clear website content cache.
        Globals::clearWebsiteContentsCache();

        $this->comment('Website content cache cleared.');

        $this->comment('Reset website done.');

        return;
    }
}
