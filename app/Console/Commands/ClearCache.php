<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ClearCache extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cc {--dev : Clear caches, regenerate and optimize helper classes}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear all caches and optimize class';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->call('cache:clear');
        $this->call('view:clear');
        $this->call('route:clear');
        $this->call('config:clear');
        $this->call('clear-compiled');

        if ($this->option('dev') == true) {
            $this->call('ide-helper:generate');
            $this->call('ide-helper:models', ['--nowrite' => 1]);
        }

        $this->comment('Clear caches done.');

        return;
    }
}
