<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Libraries\Globals;
use App\Models\AppContactUsSubmission;
use App\Models\AppWebsiteContent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use JsValidator;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $currentUrl = route('front@home');

        // Check request action.
        if ($request->isMethod('post') && $request->has('_act')) {
            $act = $request->input('_act');

            if ($act == 'do_save') {
                return $this->doSave($request, $currentUrl);
            }

            return $this->responseError404($request);
        }

        $attrNames = $this->getAttrNames();
        $jsValidator = JsValidator::make($this->getFormRules(), [], $attrNames, '#main_form');

        // Load data.
        $data = Cache::rememberForever(Globals::CACHE_KEY_WEBSITE_CONTENTS, function () {
            // Cache content.
            $data = collect([]);
            $websiteContentDBs = DB::table(AppWebsiteContent::getTableName() . ' as a')->get(['a.key', 'a.format', 'a.value']);

            foreach ($websiteContentDBs as $r) {
                switch ($r->format) {
                    case AppWebsiteContent::FORMAT_JSON:
                        $data->put($r->key, json_decode($r->value, true));
                        break;
                    case AppWebsiteContent::FORMAT_TEXT:
                        $data->put($r->key, $r->value);
                        break;
                }
            }

            return $data;
        });

        return view('front.home', compact('currentUrl', 'attrNames', 'jsValidator', 'data'));
    }

    private function doSave(Request $request, $currentUrl)
    {
        // Create rules and validate inputs.
        $attrNames = $this->getAttrNames();
        $formRules = $this->getFormRules();

        $validator = Validator::make($request->all(), $formRules, [], $attrNames);

        if ($validator->passes()) {
            // Everything is fine.
            try {
                DB::beginTransaction();

                AppContactUsSubmission::create([
                    'name' => $request->input('name'),
                    'email' => $request->input('email'),
                    'contact_no' => $request->input('contact_no'),
                    'message' => $request->input('message'),
                    'status_code' => AppContactUsSubmission::STATUS_CODE_UNREAD,
                ]);

                DB::commit();

                return redirect($currentUrl)
                    ->with(Globals::MSG_SUCCESS, Lang::get('m.record_save_success'))
                    ->withErrors($validator->messages());
            } catch (Exception $e) {
                DB::rollBack();

                Globals::logException($e);

                return redirect($currentUrl)
                    ->withInput($request->all())
                    ->withErrors(Lang::get('m.record_save_fail'));
            }
        }

        return redirect($currentUrl)
            ->withInput($request->all())
            ->withErrors($validator->messages());
    }

    /**
     * @return array
     */
    private function getAttrNames()
    {
        $attrNames = AppContactUsSubmission::getAttrNames();

        return $attrNames;
    }

    /**
     * @return array
     */
    private function getFormRules()
    {
        $rules = AppContactUsSubmission::getRules([
            'name',
            'email',
            'contact_no',
            'message',
        ]);

        return $rules;
    }
}
