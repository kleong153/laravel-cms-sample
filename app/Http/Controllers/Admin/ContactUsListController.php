<?php

namespace App\Http\Controllers\Admin;

use App\Exports\BaseExcelExport;
use App\Http\Controllers\Controller;
use App\Libraries\Globals;
use App\Libraries\JsDataTables;
use App\Models\AppContactUsSubmission;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Maatwebsite\Excel\Facades\Excel;

class ContactUsListController extends Controller
{
    public function index(Request $request)
    {
        $pageTitle = Lang::get('m.contact_us_list');
        $currentUrl = route('admin@contact_us_list');

        // Check request action.
        if ($request->isMethod('post') && $request->has('_act')) {
            $act = $request->input('_act');

            if ($act == 'do_get_list') {
                return $this->doGetList($request);
            } elseif ($act == "do_export_excel") {
                return $this->doExportExcel($request);
            }

            return $this->responseError404($request);
        }

        $attrNames = $this->getAttrNames();

        return view('admin.contactUsList', compact('pageTitle', 'currentUrl', 'attrNames'));
    }

    private function doGetList(Request $request)
    {
        $jsDataTables = new JsDataTables($request->all());
        $data = [];

        // Get records from database.
        $queryBuilder = $this->initQueryBuilder($jsDataTables, false);
        $recordsTotal = $queryBuilder->count();

        // Filter records by search params and paginate records.
        $queryBuilder = $this->filterQueryBuilder($jsDataTables, $queryBuilder);
        $recordsFiltered = $queryBuilder->count();
        $records = $jsDataTables->pagingQueryBuilder($queryBuilder)->get();

        // Extract records to array.
        $data = $this->extractRecordData($records, false);

        return $jsDataTables->resultJson($recordsTotal, $recordsFiltered, $data);
    }

    /**
     * @return array
     */
    private function getAttrNames()
    {
        $attrNames = AppContactUsSubmission::getAttrNames();

        return $attrNames;
    }

    /**
     * @param \App\Libraries\JsDataTables $jsDataTables
     * @param bool $forExport
     * @return \Illuminate\Database\Query\Builder
     */
    private function initQueryBuilder($jsDataTables, $forExport)
    {
        // Standardize function to generate query builder for show and export records.
        // Array data index must same position as jQuery DataTable at view.
        if ($forExport) {
            $selects = [
                'a.created_at', 'a.name', 'a.email',
                'a.contact_no', 'a.status_code', 'a.message',
            ];
        } else {
            $selects = [
                'a.created_at', 'a.name', 'a.email',
                'a.contact_no', 'a.status_code', 'a.id',
            ];
        }

        $builder = DB::table(AppContactUsSubmission::getTableName() . ' as a')->select($selects);

        $builder = $jsDataTables->orderByQueryBuilder($selects, $builder);

        return $builder;
    }

    /**
     * @param \App\Libraries\JsDataTables $jsDataTables
     * @param \Illuminate\Database\Query\Builder $queryBuilder
     * @return \Illuminate\Database\Query\Builder
     */
    private function filterQueryBuilder($jsDataTables, $queryBuilder)
    {
        // Standardize function to filter model for show and export records.
        $searchParams = $jsDataTables->searchParams();

        if (!empty($searchParams['name'])) {
            $queryBuilder->where('a.name', 'like', '%' . $searchParams['name'] . '%');
        }

        if (!empty($searchParams['email'])) {
            $queryBuilder->where('a.email', 'like', '%' . $searchParams['email'] . '%');
        }

        if (!empty($searchParams['contact_no'])) {
            $queryBuilder->where('a.contact_no', 'like', '%' . $searchParams['contact_no'] . '%');
        }

        if (!empty($searchParams['status_code'])) {
            $queryBuilder->where('a.status_code', '=', $searchParams['status_code']);
        } else {
            $queryBuilder->where('a.status_code', '!=', AppContactUsSubmission::STATUS_CODE_DELETED);
        }

        if (!empty($searchParams['created_at_start'])) {
            $queryBuilder->where('a.created_at', '>=', $searchParams['created_at_start'] . ' 00:00:00');
        }

        if (!empty($searchParams['created_at_end'])) {
            $queryBuilder->where('a.created_at', '<=', $searchParams['created_at_end'] . ' 23:59:59');
        }

        return $queryBuilder;
    }

    /**
     * @param mixed $records
     * @param bool $forExport
     * @return array
     */
    private function extractRecordData($records, $forExport)
    {
        // Standardize function to extract row records to array.
        $data = [];
        $statusCodeOptions = collect(AppContactUsSubmission::getStatusCodeOptions());

        // Convert record rows into array.
        // Array data index must same position as jQuery DataTable at view.
        foreach ($records as $row) {
            if ($forExport) {
                $data[] = [
                    Carbon::parse($row->created_at)->format(Globals::DATETIME_FORMAT_DEFAULT),
                    $row->name,
                    $row->email,
                    $row->contact_no,
                    $statusCodeOptions->get($row->status_code, $row->status_code),
                    $row->message,
                ];
            } else {
                $data[] = [
                    Carbon::parse($row->created_at)->format(Globals::DATETIME_FORMAT_DEFAULT),
                    $row->name,
                    $row->email,
                    $row->contact_no,
                    $statusCodeOptions->get($row->status_code, $row->status_code),
                    $row->id,
                ];
            }
        }

        return $data;
    }

    private function doExportExcel($request)
    {
        // Setup filename and column name.
        $filename = Lang::get('m.contact_us_list') . '_' . Carbon::now()->format('YmdHis') . '.xls';
        $attrNames = $this->getAttrNames();
        $headings = [
            $attrNames['created_at'],
            $attrNames['name'],
            $attrNames['email'],
            $attrNames['contact_no'],
            $attrNames['status_code'],
            $attrNames['message'],
        ];

        // Get records from database.
        $jsDataTables = new JsDataTables($request->all());
        $queryBuilder = $this->initQueryBuilder($jsDataTables, true);
        $records = $this->filterQueryBuilder($jsDataTables, $queryBuilder)->get();

        // Extract records to array.
        $data = $this->extractRecordData($records, true);

        // Export and download Excel file.
        $baseExcelDownload = new BaseExcelExport();
        $baseExcelDownload->setHeadings($headings);
        $baseExcelDownload->setData(collect($data));

        return Excel::download($baseExcelDownload, $filename);
    }
}
