<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Libraries\Globals;
use App\Models\AppWebsiteContent;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use JsValidator;

class CmsOurClientsController extends Controller
{
    private $websiteContentKeysMapping = [
        'clientItems' => 'client_items',
    ];

    public function index(Request $request)
    {
        $pageTitle = Lang::get('m.our_clients');
        $currentUrl = route('admin@cms_our_clients');

        // Check request action.
        if ($request->isMethod('post') && $request->has('_act')) {
            $act = $request->input('_act');

            if ($act == 'do_save') {
                return $this->doSave($request, $currentUrl);
            }

            return $this->responseError404($request);
        }

        $attrNames = $this->getAttrNames();
        $jsValidator = JsValidator::make($this->getFormRules(), [], $attrNames, '#main_form');

        // Load data.
        $data = collect([]);
        $websiteContentDBs = DB::table(AppWebsiteContent::getTableName() . ' as a')
            ->whereIn('a.key', array_keys($this->websiteContentKeysMapping))
            ->get(['a.key', 'a.format', 'a.value']);

        // All format is JSON. No need check for TEXT.
        foreach ($websiteContentDBs as $r) {
            $data->put($this->websiteContentKeysMapping[$r->key], json_decode($r->value, true));
        }

        return view('admin.cmsOurClients', compact('pageTitle', 'currentUrl', 'attrNames', 'jsValidator', 'data'));
    }

    private function doSave(Request $request, $currentUrl)
    {
        // Create rules and validate inputs.
        $attrNames = $this->getAttrNames();
        $formRules = $this->getFormRules();

        $validator = Validator::make($request->all(), $formRules, [], $attrNames);

        if ($validator->passes()) {
            // Everything is fine.
            try {
                DB::beginTransaction();

                // Manually check and save JSON format content.
                $clientItemsDB = AppWebsiteContent::where('key', '=', 'clientItems')
                    ->where('format', '=', AppWebsiteContent::FORMAT_JSON)
                    ->first();

                if ($clientItemsDB) {
                    $valueJson = json_decode($clientItemsDB->value, true);
                    $items = [];

                    for ($i = 0; $i < count($valueJson); $i++) {
                        $imageUrl = $valueJson[$i]['imageUrl'];

                        if ($request->hasFile('imageUrl_' . $i)) {
                            $file = $request->file('imageUrl_' . $i);
                            $fileExt = strtolower($file->getClientOriginalExtension());
                            $fileName = Globals::UPLOAD_OUR_CLIENT . '_' . Carbon::now()->format('YmdHis') . '_' . rand(1000, 9999) . '.' . $fileExt;
                            $fileSrc = Globals::UPLOAD_OUR_CLIENT . '/' . $fileName;

                            if (Storage::disk('public')->put($fileSrc, File::get($file))) {
                                // Upload success, check and delete old file.
                                Storage::disk('public')->delete(str_replace('/storage/', '', $imageUrl));

                                $imageUrl = Storage::url($fileSrc);
                            }
                        }

                        $items[] = [
                            'imageUrl' => $imageUrl,
                        ];
                    }

                    $clientItemsDB->update([
                        'value' => json_encode($items),
                    ]);
                }

                DB::commit();

                Globals::clearWebsiteContentsCache();

                return redirect($currentUrl)->with(Globals::MSG_SUCCESS, Lang::get('m.record_save_success'));
            } catch (Exception $e) {
                DB::rollBack();

                Globals::logException($e);

                return redirect($currentUrl)
                    ->withInput($request->all())
                    ->withErrors(Lang::get('m.record_save_fail'));
            }
        }

        return redirect($currentUrl)
            ->withInput($request->all())
            ->withErrors($validator->messages());
    }

    /**
     * @return array
     */
    private function getAttrNames()
    {
        $attrNames = [
            'client_items' => Lang::get('m.client_items'),

            // For JSON items.
            'imageUrl' => Lang::get('m.image_url'),
        ];

        return $attrNames;
    }

    /**
     * @return array
     */
    private function getFormRules()
    {
        $rules = [];

        return $rules;
    }
}
