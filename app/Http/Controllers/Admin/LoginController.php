<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;

class LoginController extends Controller
{
    use ThrottlesLogins;

    public function __construct()
    {
        $this->middleware('guest')->except(['logout']);
    }

    public function login(Request $request)
    {
        if (!$request->isMethod('POST')) {
            return view('admin.login');
        }

        // Code clone from built-in Illuminate\Foundation\Auth\AuthenticatesUsers.
        $rules = [
            $this->username() => ['string', 'required'],
            'password' => ['string', 'required'],
            'captcha' => ['captcha', 'required'],
        ];

        $attrNames = [
            $this->username() => Lang::get('m.username'),
            'password' => Lang::get('m.password'),
            'captcha' => Lang::get('m.captcha'),
        ];

        $this->validate($request, $rules, [], $attrNames);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $errors = [$this->username() => Lang::get('auth.failed')];

        if ($this->guard()->attempt($request->only($this->username(), 'password'), $request->has('remember'))) {
            $request->session()->regenerate();

            $this->clearLoginAttempts($request);

            // The user has been authenticated.
            return redirect()->route('admin@home');
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        if ($request->expectsJson()) {
            return response()->json($errors, 422);
        }

        return redirect()->back()
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors($errors);
    }

    public function logout(Request $request)
    {
        $guard = $this->guard();

        if (!is_null($guard)) {
            $guard->logout();
        }

        $request->session()->invalidate();

        return redirect()->route('admin@login');
    }

    /**
     * @return string
     */
    private function username()
    {
        // Method required for ThrottlesLogins.
        return 'username';
    }

    /**
     * @return \Illuminate\Contracts\Auth\Guard|\Illuminate\Contracts\Auth\StatefulGuard
     */
    private function guard()
    {
        return Auth::guard('web_admin');
    }
}
