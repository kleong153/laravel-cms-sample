<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Libraries\Globals;
use App\Models\AppWebsiteContent;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use JsValidator;

class CmsWebsiteController extends Controller
{
    private $websiteContentKeysMapping = [
        'pageTitle' => 'page_title',
        'headerLeadIn' => 'header_lead_in',
        'headerHeading' => 'header_heading',
        'copyright' => 'copyright',
        'twitterUrl' => 'twitter_url',
        'facebookUrl' => 'facebook_url',
        'linkedInUrl' => 'linkedin_url',
        'privacyPolicyUrl' => 'privacy_policy_url',
        'termsOfUseUrl' => 'terms_of_use_url',
    ];

    public function index(Request $request)
    {
        $pageTitle = Lang::get('m.website');
        $currentUrl = route('admin@cms_website');

        // Check request action.
        if ($request->isMethod('post') && $request->has('_act')) {
            $act = $request->input('_act');

            if ($act == 'do_save') {
                return $this->doSave($request, $currentUrl);
            }

            return $this->responseError404($request);
        }

        $attrNames = $this->getAttrNames();
        $jsValidator = JsValidator::make($this->getFormRules(), [], $attrNames, '#main_form');

        // Load data.
        $data = collect([]);
        $websiteContentDBs = DB::table(AppWebsiteContent::getTableName() . ' as a')
            ->whereIn('a.key', array_keys($this->websiteContentKeysMapping))
            ->get(['a.key', 'a.format', 'a.value']);

        // All format is TEXT. No need check for JSON.
        foreach ($websiteContentDBs as $r) {
            $data->put($this->websiteContentKeysMapping[$r->key], $r->value);
        }

        return view('admin.cmsWebsite', compact('pageTitle', 'currentUrl', 'attrNames', 'jsValidator', 'data'));
    }

    private function doSave(Request $request, $currentUrl)
    {
        // Create rules and validate inputs.
        $attrNames = $this->getAttrNames();
        $formRules = $this->getFormRules();

        $validator = Validator::make($request->all(), $formRules, [], $attrNames);

        if ($validator->passes()) {
            // Everything is fine.
            try {
                DB::beginTransaction();

                // All format is TEXT. No need check for JSON.
                // Only auto save TEXT format contents.
                foreach ($this->websiteContentKeysMapping as $key => $val) {
                    if ($request->has($val)) {
                        $input = Globals::ifNull($request->input($val), '');

                        AppWebsiteContent::where('key', '=', $key)
                            ->where('format', '=', AppWebsiteContent::FORMAT_TEXT)
                            ->update([
                                'value' => $input,
                            ]);
                    }
                }

                DB::commit();

                Globals::clearWebsiteContentsCache();

                return redirect($currentUrl)->with(Globals::MSG_SUCCESS, Lang::get('m.record_save_success'));
            } catch (Exception $e) {
                DB::rollBack();

                Globals::logException($e);

                return redirect($currentUrl)
                    ->withInput($request->all())
                    ->withErrors(Lang::get('m.record_save_fail'));
            }
        }

        return redirect($currentUrl)
            ->withInput($request->all())
            ->withErrors($validator->messages());
    }

    /**
     * @return array
     */
    private function getAttrNames()
    {
        $attrNames = [
            'page_title' => Lang::get('m.page_title'),
            'header_lead_in' => Lang::get('m.header_lead_in'),
            'header_heading' => Lang::get('m.header_heading'),
            'copyright' => Lang::get('m.copyright'),
            'twitter_url' => Lang::get('m.twitter_url'),
            'facebook_url' => Lang::get('m.facebook_url'),
            'linkedin_url' => Lang::get('m.linkedin_url'),
            'privacy_policy_url' => Lang::get('m.privacy_policy_url'),
            'terms_of_use_url' => Lang::get('m.terms_of_use_url'),
        ];

        return $attrNames;
    }

    /**
     * @return array
     */
    private function getFormRules()
    {
        $rules = [];

        return $rules;
    }
}
