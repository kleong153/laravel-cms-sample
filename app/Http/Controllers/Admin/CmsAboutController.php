<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Libraries\Globals;
use App\Models\AppWebsiteContent;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use JsValidator;

class CmsAboutController extends Controller
{
    private $websiteContentKeysMapping = [
        'aboutHeading' => 'about_heading',
        'aboutItems' => 'about_items',
    ];

    public function index(Request $request)
    {
        $pageTitle = Lang::get('m.about');
        $currentUrl = route('admin@cms_about');

        // Check request action.
        if ($request->isMethod('post') && $request->has('_act')) {
            $act = $request->input('_act');

            if ($act == 'do_save') {
                return $this->doSave($request, $currentUrl);
            }

            return $this->responseError404($request);
        }

        $attrNames = $this->getAttrNames();
        $jsValidator = JsValidator::make($this->getFormRules(), [], $attrNames, '#main_form');

        // Load data.
        $data = collect([]);
        $websiteContentDBs = DB::table(AppWebsiteContent::getTableName() . ' as a')
            ->whereIn('a.key', array_keys($this->websiteContentKeysMapping))
            ->get(['a.key', 'a.format', 'a.value']);

        foreach ($websiteContentDBs as $r) {
            $value = null;

            if ($r->format == AppWebsiteContent::FORMAT_TEXT) {
                $value = $r->value;
            } elseif ($r->format == AppWebsiteContent::FORMAT_JSON) {
                $value = json_decode($r->value, true);
            }

            if (!is_null($value)) {
                $data->put($this->websiteContentKeysMapping[$r->key], $value);
            }
        }

        $directionOptions = $this->getDirectionOptions();

        return view('admin.cmsAbout', compact('pageTitle', 'currentUrl', 'attrNames', 'jsValidator', 'data', 'directionOptions'));
    }

    private function doSave(Request $request, $currentUrl)
    {
        // Create rules and validate inputs.
        $attrNames = $this->getAttrNames();
        $formRules = $this->getFormRules();

        $validator = Validator::make($request->all(), $formRules, [], $attrNames);

        if ($validator->passes()) {
            // Everything is fine.
            try {
                DB::beginTransaction();

                // Only auto save TEXT format contents.
                foreach ($this->websiteContentKeysMapping as $key => $val) {
                    if ($request->has($val)) {
                        $input = $request->input($val);

                        if (is_null($input)) {
                            $input = '';
                        }

                        AppWebsiteContent::where('key', '=', $key)
                            ->where('format', '=', AppWebsiteContent::FORMAT_TEXT)
                            ->update([
                                'value' => $input,
                            ]);
                    }
                }

                // Manually check and save JSON format content.
                $aboutItemsDB = AppWebsiteContent::where('key', '=', 'aboutItems')
                    ->where('format', '=', AppWebsiteContent::FORMAT_JSON)
                    ->first();

                if ($aboutItemsDB) {
                    $valueJson = json_decode($aboutItemsDB->value, true);
                    $items = [];

                    for ($i = 0; $i < count($valueJson); $i++) {
                        $imageUrl = $valueJson[$i]['imageUrl'];

                        if ($request->hasFile('imageUrl_' . $i)) {
                            $file = $request->file('imageUrl_' . $i);
                            $fileExt = strtolower($file->getClientOriginalExtension());
                            $fileName = Globals::UPLOAD_ABOUT . '_' . Carbon::now()->format('YmdHis') . '_' . rand(1000, 9999) . '.' . $fileExt;
                            $fileSrc = Globals::UPLOAD_ABOUT . '/' . $fileName;

                            if (Storage::disk('public')->put($fileSrc, File::get($file))) {
                                // Upload success, check and delete old file.
                                Storage::disk('public')->delete(str_replace('/storage/', '', $imageUrl));

                                $imageUrl = Storage::url($fileSrc);
                            }
                        }

                        $items[] = [
                            'time' => Globals::ifNull($request->input('time_' . $i), ''),
                            'title' => Globals::ifNull($request->input('title_' . $i), ''),
                            'content' => Globals::ifNull($request->input('content_' . $i), ''),
                            'imageUrl' => $imageUrl,
                            'direction' => Globals::ifNull($request->input('direction_' . $i), ''),
                        ];
                    }

                    $aboutItemsDB->update([
                        'value' => json_encode($items),
                    ]);
                }

                DB::commit();

                Globals::clearWebsiteContentsCache();

                return redirect($currentUrl)->with(Globals::MSG_SUCCESS, Lang::get('m.record_save_success'));
            } catch (Exception $e) {
                DB::rollBack();

                Globals::logException($e);

                return redirect($currentUrl)
                    ->withInput($request->all())
                    ->withErrors(Lang::get('m.record_save_fail'));
            }
        }

        return redirect($currentUrl)
            ->withInput($request->all())
            ->withErrors($validator->messages());
    }

    /**
     * @return array
     */
    private function getAttrNames()
    {
        $attrNames = [
            'about_heading' => Lang::get('m.about_heading'),
            'about_items' => Lang::get('m.about_items'),

            // For JSON items.
            'time' => Lang::get('m.time'),
            'title' => Lang::get('m.title'),
            'content' => Lang::get('m.content'),
            'imageUrl' => Lang::get('m.image_url'),
            'direction' => Lang::get('m.direction'),
        ];

        return $attrNames;
    }

    /**
     * @return array
     */
    private function getFormRules()
    {
        $rules = [];

        return $rules;
    }

    /**
     * @return array
     */
    private function getDirectionOptions()
    {
        return [
            'left' => Lang::get('m.left'),
            'right' => Lang::get('m.right'),
        ];
    }
}
