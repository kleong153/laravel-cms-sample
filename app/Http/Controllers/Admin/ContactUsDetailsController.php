<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Libraries\Globals;
use App\Models\AppContactUsSubmission;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use JsValidator;

class ContactUsDetailsController extends Controller
{
    public function index(Request $request)
    {
        $pageTitle = Lang::get('m.contact_us_details');
        $currentUrl = route('admin@contact_us_details');
        $backUrl = route('admin@contact_us_list');

        // Check request action.
        if ($request->isMethod('post') && $request->has('_act')) {
            $act = $request->input('_act');

            if ($act == 'do_save') {
                return $this->doSave($request, $currentUrl, $backUrl);
            }

            return $this->responseError404($request);
        }

        $attrNames = $this->getAttrNames();
        $jsValidator = JsValidator::make($this->getFormRules(), [], $attrNames, '#main_form');

        $statusCodeOptions = $this->getStatusCodeOptions();

        // Default values.
        $data = collect([
            'id' => $request->input('id'),
        ]);

        if (!empty($data->get('id'))) {
            // Load data.
            $contactUsSubDB = AppContactUsSubmission::find($data->get('id'));

            if ($contactUsSubDB) {
                $data = $data->merge([
                    'name' => $contactUsSubDB->name,
                    'email' => $contactUsSubDB->email,
                    'contact_no' => $contactUsSubDB->contact_no,
                    'message' => $contactUsSubDB->message,
                    'status_code' => $contactUsSubDB->status_code,
                    'created_at' => Carbon::parse($contactUsSubDB->created_at)->format(Globals::DATETIME_FORMAT_DEFAULT),
                    'updated_at' => Carbon::parse($contactUsSubDB->updated_at)->format(Globals::DATETIME_FORMAT_DEFAULT),
                ]);

                return view('admin.contactUsDetails', compact('pageTitle', 'currentUrl', 'backUrl', 'attrNames', 'jsValidator', 'data', 'statusCodeOptions'));
            }
        }

        return redirect($backUrl)->with(Globals::MSG_ERROR, Lang::get('m.record_not_found'));
    }

    private function doSave(Request $request, $currentUrl, $backUrl)
    {
        $id = $request->input('id');
        $currentUrl2 = $currentUrl . '?id=' . $id; // Current URL with query string.
        $contactUsSubDB = null;

        if (!empty($id)) {
            $contactUsSubDB = AppContactUsSubmission::find($id);
        }

        if (!$contactUsSubDB) {
            return redirect($backUrl)->with(Globals::MSG_ERROR, Lang::get('m.record_not_found'));
        }

        // Create rules and validate inputs.
        $attrNames = $this->getAttrNames();
        $formRules = $this->getFormRules();

        $validator = Validator::make($request->all(), $formRules, [], $attrNames);

        if ($validator->passes()) {
            // Second level validation.
            $invalidOptions = [];

            if (!array_key_exists($request->input('status_code'), $this->getStatusCodeOptions())) {
                $invalidOptions['status_code'] = $attrNames['status_code'];
            }

            // Compose errors to validator.
            Globals::validatorComposeError($validator, $invalidOptions);

            if ($validator->errors()->count() === 0) {
                // Everything is fine.
                try {
                    DB::beginTransaction();

                    $contactUsSubDB->update([
                        'status_code' => $request->input('status_code'),
                    ]);

                    DB::commit();

                    return redirect($currentUrl . '?id=' . $contactUsSubDB->id)->with(Globals::MSG_SUCCESS, Lang::get('m.record_save_success'));
                } catch (Exception $e) {
                    DB::rollBack();

                    Globals::logException($e);

                    return redirect($currentUrl2)
                        ->withInput($request->all())
                        ->withErrors(Lang::get('m.record_save_fail'));
                }
            }
        }

        return redirect($currentUrl2)
            ->withInput($request->all())
            ->withErrors($validator->messages());
    }

    /**
     * @return array
     */
    private function getAttrNames()
    {
        $attrNames = AppContactUsSubmission::getAttrNames();

        return $attrNames;
    }

    /**
     * @return array
     */
    private function getFormRules()
    {
        $rules = AppContactUsSubmission::getRules(['status_code']);

        return $rules;
    }

    /**
     * @return array
     */
    private function getStatusCodeOptions()
    {
        return AppContactUsSubmission::getStatusCodeOptions();
    }
}
