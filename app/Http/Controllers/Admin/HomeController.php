<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $pageTitle = Lang::get('m.dashboard');
        $currentUrl = route('admin@home');
        $data = [];

        return view('admin.home', compact('pageTitle', 'currentUrl', 'data'));
    }
}
