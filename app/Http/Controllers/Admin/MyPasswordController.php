<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Libraries\Globals;
use App\Models\AppAdmin;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use JsValidator;

class MyPasswordController extends Controller
{
    public function index(Request $request)
    {
        $pageTitle = Lang::get('m.password_settings');
        $currentUrl = route('admin@password_settings');

        // Check request action.
        if ($request->isMethod('post') && $request->has('_act')) {
            $act = $request->input('_act');

            if ($act == 'do_save') {
                return $this->doSave($request, $currentUrl);
            }

            return $this->responseError404($request);
        }

        $attrNames = $this->getAttrNames();
        $jsValidator = JsValidator::make($this->getFormRules(), [], $attrNames, '#main_form');

        return view('admin.passwordSettings', compact('pageTitle', 'currentUrl', 'attrNames', 'jsValidator'));
    }

    private function doSave(Request $request, $currentUrl)
    {
        $authUser = $request->user();

        // Create rules and validate inputs.
        $attrNames = $this->getAttrNames();
        $formRules = $this->getFormRules();

        $validator = Validator::make($request->all(), $formRules, [], $attrNames);

        if ($validator->passes()) {
            // Second level validation.
            $invalidOptions = [];

            if (!Hash::check($request->input('password_old'), $authUser->password)) {
                $validator->errors()->add('password_old', Lang::get('m.invalid_old_password'));
            }

            // Compose errors to validator.
            Globals::validatorComposeError($validator, $invalidOptions);

            if ($validator->errors()->count() === 0) {
                // Everything is fine.
                try {
                    DB::beginTransaction();

                    $authUser->update([
                        'password' => Hash::make($request->input('password_new')),
                    ]);

                    DB::commit();

                    return redirect($currentUrl)->with(Globals::MSG_SUCCESS, Lang::get('m.record_save_success'));
                } catch (Exception $e) {
                    DB::rollBack();

                    Globals::logException($e);

                    return redirect($currentUrl)
                        ->withInput($request->all())
                        ->withErrors(Lang::get('m.record_save_fail'));
                }
            }
        }

        return redirect($currentUrl)
            ->withInput($request->all())
            ->withErrors($validator->messages());
    }

    /**
     * @return array
     */
    private function getAttrNames()
    {
        $attrNames = AppAdmin::getAttrNames();

        return $attrNames;
    }

    /**
     * @return array
     */
    private function getFormRules()
    {
        $rules = AppAdmin::getRules([
            'password_old',
            'password_new',
        ]);

        $rules['password_old'][] = 'required';
        $rules['password_new'][] = 'required';

        return $rules;
    }
}
