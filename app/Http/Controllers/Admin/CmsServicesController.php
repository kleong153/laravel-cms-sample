<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Libraries\Globals;
use App\Models\AppWebsiteContent;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use JsValidator;

class CmsServicesController extends Controller
{
    private $websiteContentKeysMapping = [
        'serviceHeading' => 'service_heading',
        'serviceItems' => 'service_items',
    ];

    public function index(Request $request)
    {
        $pageTitle = Lang::get('m.services');
        $currentUrl = route('admin@cms_services');

        // Check request action.
        if ($request->isMethod('post') && $request->has('_act')) {
            $act = $request->input('_act');

            if ($act == 'do_save') {
                return $this->doSave($request, $currentUrl);
            }

            return $this->responseError404($request);
        }

        $attrNames = $this->getAttrNames();
        $jsValidator = JsValidator::make($this->getFormRules(), [], $attrNames, '#main_form');

        // Load data.
        $data = collect([]);
        $websiteContentDBs = DB::table(AppWebsiteContent::getTableName() . ' as a')
            ->whereIn('a.key', array_keys($this->websiteContentKeysMapping))
            ->get(['a.key', 'a.format', 'a.value']);

        foreach ($websiteContentDBs as $r) {
            $value = null;

            if ($r->format == AppWebsiteContent::FORMAT_TEXT) {
                $value = $r->value;
            } elseif ($r->format == AppWebsiteContent::FORMAT_JSON) {
                $value = json_decode($r->value, true);
            }

            if (!is_null($value)) {
                $data->put($this->websiteContentKeysMapping[$r->key], $value);
            }
        }

        return view('admin.cmsServices', compact('pageTitle', 'currentUrl', 'attrNames', 'jsValidator', 'data'));
    }

    private function doSave(Request $request, $currentUrl)
    {
        // Create rules and validate inputs.
        $attrNames = $this->getAttrNames();
        $formRules = $this->getFormRules();

        $validator = Validator::make($request->all(), $formRules, [], $attrNames);

        if ($validator->passes()) {
            // Everything is fine.
            try {
                DB::beginTransaction();

                // Only auto save TEXT format contents.
                foreach ($this->websiteContentKeysMapping as $key => $val) {
                    if ($request->has($val)) {
                        $input = $request->input($val);

                        if (is_null($input)) {
                            $input = '';
                        }

                        AppWebsiteContent::where('key', '=', $key)
                            ->where('format', '=', AppWebsiteContent::FORMAT_TEXT)
                            ->update([
                                'value' => $input,
                            ]);
                    }
                }

                // Manually check and save JSON format content.
                $items = [];

                for ($i = 0; $i < 3; $i++) {
                    $items[] = [
                        'icon' => Globals::ifNull($request->input('icon_' . $i), ''),
                        'title' => Globals::ifNull($request->input('title_' . $i), ''),
                        'content' => Globals::ifNull($request->input('content_' . $i), ''),
                    ];
                }

                AppWebsiteContent::where('key', '=', 'serviceItems')
                    ->where('format', '=', AppWebsiteContent::FORMAT_JSON)
                    ->update([
                        'value' => json_encode($items),
                    ]);

                DB::commit();

                Globals::clearWebsiteContentsCache();

                return redirect($currentUrl)->with(Globals::MSG_SUCCESS, Lang::get('m.record_save_success'));
            } catch (Exception $e) {
                DB::rollBack();

                Globals::logException($e);

                return redirect($currentUrl)
                    ->withInput($request->all())
                    ->withErrors(Lang::get('m.record_save_fail'));
            }
        }

        return redirect($currentUrl)
            ->withInput($request->all())
            ->withErrors($validator->messages());
    }

    /**
     * @return array
     */
    private function getAttrNames()
    {
        $attrNames = [
            'service_heading' => Lang::get('m.service_heading'),
            'service_items' => Lang::get('m.service_items'),

            // For JSON items.
            'icon' => Lang::get('m.icon'),
            'title' => Lang::get('m.title'),
            'content' => Lang::get('m.content'),
        ];

        return $attrNames;
    }

    /**
     * @return array
     */
    private function getFormRules()
    {
        $rules = [];

        return $rules;
    }
}
