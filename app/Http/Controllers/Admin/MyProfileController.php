<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Libraries\Globals;
use App\Models\AppAdmin;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use JsValidator;

class MyProfileController extends Controller
{
    public function index(Request $request)
    {
        $pageTitle = Lang::get('m.profile_settings');
        $currentUrl = route('admin@profile_settings');

        // Check request action.
        if ($request->isMethod('post') && $request->has('_act')) {
            $act = $request->input('_act');

            if ($act == 'do_save') {
                return $this->doSave($request, $currentUrl);
            }

            return $this->responseError404($request);
        }

        $attrNames = $this->getAttrNames();
        $jsValidator = JsValidator::make($this->getFormRules(), [], $attrNames, '#main_form');

        // Load data.
        $adminDB = $request->user();

        $data = collect([
            'username' => $adminDB->username,
            'email' => $adminDB->email,
            'created_at' => Carbon::parse($adminDB->created_at)->format(Globals::DATETIME_FORMAT_DEFAULT),
            'updated_at' => Carbon::parse($adminDB->updated_at)->format(Globals::DATETIME_FORMAT_DEFAULT),
        ]);

        return view('admin.profileSettings', compact('pageTitle', 'currentUrl', 'attrNames', 'jsValidator', 'data'));
    }

    private function doSave(Request $request, $currentUrl)
    {
        $authUser = $request->user();

        // Create rules and validate inputs.
        $attrNames = $this->getAttrNames();
        $formRules = $this->getFormRules();

        $validator = Validator::make($request->all(), $formRules, [], $attrNames);

        if ($validator->passes()) {
            // Second level validation.
            $invalidOptions = [];

            $isEmailInUse = AppAdmin::where('email', '=', $request->input('email'))
                ->where('id', '!=', $authUser->id)
                ->first(['id']);

            if ($isEmailInUse) {
                $validator->errors()->add('email', Lang::get('m.email_is_already_in_use'));
            }

            // Compose errors to validator.
            Globals::validatorComposeError($validator, $invalidOptions);

            if ($validator->errors()->count() === 0) {
                // Everything is fine.
                try {
                    DB::beginTransaction();

                    $authUser->update([
                        'email' => $request->input('email'),
                    ]);

                    DB::commit();

                    return redirect($currentUrl)->with(Globals::MSG_SUCCESS, Lang::get('m.record_save_success'));
                } catch (Exception $e) {
                    DB::rollBack();

                    Globals::logException($e);

                    return redirect($currentUrl)
                        ->withInput($request->all())
                        ->withErrors(Lang::get('m.record_save_fail'));
                }
            }
        }

        return redirect($currentUrl)
            ->withInput($request->all())
            ->withErrors($validator->messages());
    }

    /**
     * @return array
     */
    private function getAttrNames()
    {
        $attrNames = AppAdmin::getAttrNames();

        return $attrNames;
    }

    /**
     * @return array
     */
    private function getFormRules()
    {
        $rules = AppAdmin::getRules(['email']);

        return $rules;
    }
}
