<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Session\TokenMismatchException;
use Lang;
use Session;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    protected function responseError404(Request $request)
    {
        if ($request->isJson() || $request->wantsJson()) {
            return response(Lang::get('m.page_not_found'), 404);
        } else {
            abort(404);
        }
    }

    /**
     * @param Request $request
     * @param string $token
     * @throws TokenMismatchException
     */
    protected function checkCsrfToken(Request $request, $token = null)
    {
        if (is_null($token)) {
            $token = $request->input('_token');
        }

        if (Session::token() != $token) {
            throw new TokenMismatchException();
        }
    }

    /**
     * @param int $code
     * @param string $message
     * @param array $data
     * @param int $status
     * @return \Illuminate\Http\JsonResponse
     */
    protected function responseJson($code = 200, $message = '', $data = null, $status = 200)
    {
        $hasData = (is_null($data) || empty($data) ? 'n' : 'y');

        if (is_null($data)) {
            $data = new \stdClass();
        }

        return response()->json([
            'code' => $code,
            'message' => $message,
            'has_data' => $hasData,
            'data' => $data
        ], $status, $this->getStandardResponseJsonHeaders(), JSON_UNESCAPED_UNICODE);
    }

    /**
     * @param array $jsonData
     * @param array $requiredKeys
     * @return bool
     */
    protected function validateJsonRequestData($jsonData, $requiredKeys)
    {
        if (!is_array($jsonData)) {
            return false;
        }

        foreach ($requiredKeys as $key) {
            if (!array_key_exists($key, $jsonData)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param \Illuminate\Validation\Validator $validator
     * @param bool $sign
     * @return \Illuminate\Http\JsonResponse
     */
    protected function responseJsonValidationFailed($validator, $sign = true)
    {
        $errors = [];

        foreach ($validator->errors()->getMessages() as $key => $value) {
            $errors[] = [$key => $value[0]];
        }

        return $this->responseJson(801, Lang::get('m.validation_failed'), ['errors' => $errors], $sign);
    }

    /**
     * @param Request $request
     * @return array|mixed
     */
    protected function getJsonDataArr(Request $request)
    {
        $data = json_decode($request->input('data'), true);

        if (is_null($data)) {
            return [];
        }

        return $data;
    }

    protected function getStandardResponseJsonHeaders()
    {
        return [
            'Content-Type' => 'application/json;charset=UTF-8',
            'charset' => 'utf-8',
        ];
    }
}
