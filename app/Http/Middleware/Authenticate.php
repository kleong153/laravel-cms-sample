<?php

namespace App\Http\Middleware;

use App\Models\AppAdmin;
use App\Models\AppUser;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Auth\Middleware\Authenticate as BaseAuthenticate;

class Authenticate extends BaseAuthenticate
{
    /**
     * Determine if the user is logged in to any of the given guards.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  array  $guards
     * @return void
     *
     * @throws \Illuminate\Auth\AuthenticationException
     */
    protected function authenticate($request, array $guards)
    {
        if (empty($guards)) {
            return $this->auth->authenticate();
        }

        foreach ($guards as $guard) {
            $guardName = 'web'; // Follow settings in config/auth.php guards.
            $accountStatusCodeActive = AppUser::STATUS_CODE_ACTIVE; // Default = AppUser.

            if (!empty($guard)) {
                // Sample format: web, web_admin...
                $guardName = $guard;

                if ($guard == 'web_admin') {
                    $accountStatusCodeActive = AppAdmin::STATUS_CODE_ACTIVE;
                }
            }

            if ($this->auth->guard($guardName)->check()) {
                $user = $this->auth->guard($guardName)->user();

                if ($user->{'status_code'} == $accountStatusCodeActive) {
                    return $this->auth->shouldUse($guardName);
                }
            }
        }

        throw new AuthenticationException('Unauthenticated.', $guards);
    }
}
