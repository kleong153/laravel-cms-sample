<?php

namespace App\Models;

use Illuminate\Support\Arr;

class AppWebsiteContent extends BaseModel
{

    protected $table = 'app_website_contents';
    protected $guarded = [];

    const FORMAT_TEXT = 'TEXT';
    const FORMAT_JSON = 'JSON';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * @param array $only
     * @return array
     */
    public static function getRules($only = [])
    {
        $rules = [
            'key' => ['string', 'required', 'max:180'],
            'value' => ['string', 'max:4000'],
        ];

        if (!empty($only)) {
            return Arr::only($rules, $only);
        }

        return $rules;
    }

    /**
     * @param array $only
     * @return array
     */
    public static function getAttrNames($only = [])
    {
        $attrNames = [];

        if (!empty($only)) {
            return Arr::only($attrNames, $only);
        }

        return $attrNames;
    }
}
