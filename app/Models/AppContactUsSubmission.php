<?php

namespace App\Models;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Lang;

class AppContactUsSubmission extends BaseModel
{

    protected $table = 'app_contact_us_submissions';
    protected $guarded = [];

    const STATUS_CODE_UNREAD = 'UNREAD';
    const STATUS_CODE_READ = 'READ';
    const STATUS_CODE_DELETED = 'DELETED';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * @param array $only
     * @return array
     */
    public static function getRules($only = [])
    {
        $rules = [
            'name' => ['string', 'required', 'max:180'],
            'email' => ['email', 'required', 'max:180'],
            'contact_no' => ['string', 'required', 'max:50'],
            'message' => ['string', 'required', 'max:1000'],
            'status_code' => ['string', 'required'],
        ];

        if (!empty($only)) {
            return Arr::only($rules, $only);
        }

        return $rules;
    }

    /**
     * @param array $only
     * @return array
     */
    public static function getAttrNames($only = [])
    {
        $attrNames = [
            'name' => Lang::get('m.name'),
            'email' => Lang::get('m.email'),
            'contact_no' => Lang::get('m.contact_no'),
            'message' => Lang::get('m.message'),
            'status_code' => Lang::get('m.status'),
            'created_at' => Lang::get('m.submitted_at'),
            'updated_at' => Lang::get('m.last_modified_date'),
        ];

        if (!empty($only)) {
            return Arr::only($attrNames, $only);
        }

        return $attrNames;
    }

    /**
     * @param boolean $prependOptionAll
     * @return array
     */
    public static function getStatusCodeOptions($prependOptionAll = false)
    {
        $options = [];

        if ($prependOptionAll) {
            $options[''] = Lang::get('m.all');
        }

        $options += [
            self::STATUS_CODE_UNREAD => Lang::get('m.unread'),
            self::STATUS_CODE_READ => Lang::get('m.read'),
            self::STATUS_CODE_DELETED => Lang::get('m.deleted'),
        ];

        return $options;
    }
}
