<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\MustVerifyEmail;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Lang;

class AppUser extends BaseModel implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{

    use Authenticatable, Authorizable, CanResetPassword, MustVerifyEmail, Notifiable;

    protected $table = 'app_users';
    protected $guarded = [];

    const STATUS_CODE_ACTIVE = 'ACTIVE';
    const STATUS_CODE_SUSPENDED = 'SUSPENDED';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @param array $only
     * @return array
     */
    public static function getRules($only = [])
    {
        $rules = [
            'username' => ['string', 'required', 'between:8,20', 'regex:/^[a-zA-Z0-9_]+$/'],
            'email' => ['email', 'required', 'max:160'],
            'password' => ['string', 'required', 'confirmed', 'string', 'between:6,18'],
            'password_old' => ['string', 'between:6,18'],
            'password_new' => ['string', 'required_with:password_old', 'confirmed', 'between:6,18'],
            'status_code' => ['string', 'required'],
        ];

        if (!empty($only)) {
            return Arr::only($rules, $only);
        }

        return $rules;
    }

    /**
     * @param array $only
     * @return array
     */
    public static function getAttrNames($only = [])
    {
        $attrNames = [
            'username' => Lang::get('m.username'),
            'email' => Lang::get('m.email'),
            'password' => Lang::get('m.password'),
            'password_confirmation' => Lang::get('m.confirm_password'),
            'password_old' => Lang::get('m.old_password'),
            'password_new' => Lang::get('m.new_password'),
            'password_new_confirmation' => Lang::get('m.confirm_new_password'),
            'status_code' => Lang::get('m.account_status'),
            'created_at' => Lang::get('m.created_date'),
            'updated_at' => Lang::get('m.last_modified_date'),
        ];

        if (!empty($only)) {
            return Arr::only($attrNames, $only);
        }

        return $attrNames;
    }

    /**
     * @param boolean $prependOptionAll
     * @return array
     */
    public static function getStatusCodeOptions($prependOptionAll = false)
    {
        $options = [];

        if ($prependOptionAll) {
            $options[''] = Lang::get('m.all');
        }

        $options += [
            self::STATUS_CODE_ACTIVE => Lang::get('m.active'),
            self::STATUS_CODE_SUSPENDED => Lang::get('m.suspended'),
        ];

        return $options;
    }
}
