<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Events\AfterSheet;

class BaseExcelExport implements
    FromCollection,
    ShouldAutoSize,
    WithEvents,
    WithHeadings
{

    private $headings = [];
    private $data = null;

    /**
     * @param array $headings
     */
    public function setHeadings($headings)
    {
        $this->headings = $headings;
    }

    /**
     * @param \Illuminate\Support\Collection $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return $this->data;
    }

    public function registerEvents(): array
    {
        $headingCount = count($this->headings);

        return [
            AfterSheet::class => function (AfterSheet $event) use ($headingCount) {
                $styleArray = [
                    'font' => [
                        'bold' => true,
                    ],
                ];

                $col = $this->toAlpha($headingCount);

                // Format header.
                $event->sheet->getDelegate()
                    ->getStyle('A1:' . $col . '1')
                    ->applyFromArray($styleArray);

                // Reset selected cell.
                $event->sheet->getDelegate()->getStyle('A1');
            },
        ];
    }

    public function headings(): array
    {
        return $this->headings;
    }

    /**
     * @param int $num
     * @return string
     */
    private function toAlpha($num)
    {
        $alphabet = range('A', 'Z');
        $alphabetCount = count($alphabet);

        if ($num <= ($alphabetCount - 1)) {
            return $alphabet[$num];
        } elseif ($num > ($alphabetCount - 1)) {
            $dividend = ($num + 1);
            $alpha = '';

            while ($dividend > 0) {
                $modulo = ($dividend - 1) % $alphabetCount;
                $alpha = $alphabet[$modulo] . $alpha;
                $dividend = floor((($dividend - $modulo) / $alphabetCount));
            }

            return $alpha;
        }
    }
}
