<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\AppAdmin
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string|null $remember_token
 * @property string $status_code
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppAdmin newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppAdmin newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppAdmin query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppAdmin whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppAdmin whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppAdmin whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppAdmin wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppAdmin whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppAdmin whereStatusCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppAdmin whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppAdmin whereUsername($value)
 */
	class AppAdmin extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\AppContactUsSubmission
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $contact_no
 * @property string $message
 * @property string $status_code
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppContactUsSubmission newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppContactUsSubmission newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppContactUsSubmission query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppContactUsSubmission whereContactNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppContactUsSubmission whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppContactUsSubmission whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppContactUsSubmission whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppContactUsSubmission whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppContactUsSubmission whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppContactUsSubmission whereStatusCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppContactUsSubmission whereUpdatedAt($value)
 */
	class AppContactUsSubmission extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\AppUser
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string|null $remember_token
 * @property string $status_code
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppUser query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppUser whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppUser whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppUser wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppUser whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppUser whereStatusCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppUser whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppUser whereUsername($value)
 */
	class AppUser extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\AppWebsiteContent
 *
 * @property int $id
 * @property string $key
 * @property string $format
 * @property string $value
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppWebsiteContent newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppWebsiteContent newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppWebsiteContent query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppWebsiteContent whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppWebsiteContent whereFormat($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppWebsiteContent whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppWebsiteContent whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppWebsiteContent whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AppWebsiteContent whereValue($value)
 */
	class AppWebsiteContent extends \Eloquent {}
}

