<?php

Route::get('/', function () {
    if (Auth::guard('web_admin')->check()) {
        return redirect()->route('admin@home');
    }

    return redirect()->route('admin@login');
});

Route::match(['get', 'post'], 'login', 'Admin\LoginController@login')->name('admin@login');

Route::match(['get', 'post'], 'logout', 'Admin\LoginController@logout')->name('admin@logout');

Route::middleware('auth:web_admin')->group(function () {

    Route::match(['get', 'post'], 'home', 'Admin\HomeController@index')->name('admin@home');

    Route::match(['get', 'post'], 'profile-settings', 'Admin\MyProfileController@index')->name('admin@profile_settings');

    Route::match(['get', 'post'], 'password-settings', 'Admin\MyPasswordController@index')->name('admin@password_settings');

    Route::match(['get', 'post'], 'cms-website', 'Admin\CmsWebsiteController@index')->name('admin@cms_website');

    Route::match(['get', 'post'], 'cms-services', 'Admin\CmsServicesController@index')->name('admin@cms_services');

    Route::match(['get', 'post'], 'cms-portfolio', 'Admin\CmsPortfolioController@index')->name('admin@cms_portfolio');

    Route::match(['get', 'post'], 'cms-about', 'Admin\CmsAboutController@index')->name('admin@cms_about');

    Route::match(['get', 'post'], 'cms-our-team', 'Admin\CmsOurTeamController@index')->name('admin@cms_our_team');

    Route::match(['get', 'post'], 'cms-our-clients', 'Admin\CmsOurClientsController@index')->name('admin@cms_our_clients');

    Route::match(['get', 'post'], 'contact-us-list', 'Admin\ContactUsListController@index')->name('admin@contact_us_list');
    Route::match(['get', 'post'], 'contact-us-list/details', 'Admin\ContactUsDetailsController@index')->name('admin@contact_us_details');
});
