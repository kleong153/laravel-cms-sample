<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::match(['get', 'post'], '/', 'Front\HomeController@index')->name('front@home');

Route::get('/get_captcha/{config?}', function (\Mews\Captcha\Captcha $captcha, $config = 'default') {
    return $captcha->src($config);
})->name('get_captcha');

Route::get('/language/{lang_code}', function ($lang_code) {
    if (in_array($lang_code, config('app.supported_locale'))) {
        Session::put('language', $lang_code);
        Lang::setLocale($lang_code);

        if (Auth::guard()->check()) {
            $user = Auth::guard()->user();

            $user->update([
                'language' => $lang_code,
                'updated_by' => $user->id,
            ]);
        }
    }

    return redirect()->back();
})->name('language');

Route::get('/dev/reset-website', function () {
    \Artisan::call('website:reset');

    return response('ok');
});
