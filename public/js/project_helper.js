/**
 * Auto run.
 */
$(function() {
    initCsrfToken();
    initAutoTableTfootColSpan();
    initValidatorErrorKeysHighlight();
});

/**
 * Bootstrap DatePicker helper.
 */
function defaultDatePickerOptions() {
    return {
        format: "yyyy-mm-dd",
        todayHighlight: true,
        todayBtn: "linked",
        clearBtn: true,
        zIndexOffset: 110
    };
}

/**
 * Helper class to auto inject colspan to table tfoot.
 */
function initAutoTableTfootColSpan() {
    $("table.table-auto-tfoot-colspan").each(function() {
        var tableObj = $(this);
        var tableTfootTdObj = $("tfoot > tr > td", tableObj);
        var colCount = $("thead > tr > th").length;

        if (colCount > 0) {
            tableTfootTdObj.attr("colspan", colCount);
        } else {
            tableTfootTdObj.removeAttr("colspan");
        }
    });
}

/**
 * Helper class to highlight all error fields after validating from backend.
 */
function initValidatorErrorKeysHighlight() {
    var validatorErrorKeysObj = $("#validator_error_keys");

    if (validatorErrorKeysObj.length) {
        var keys = validatorErrorKeysObj.val().split("|");
        var keyObj = "";

        for (var i = 0; i < keys.length; i++) {
            keyObj = $("#" + keys[i]);

            if (keyObj.length) {
                keyObj.parent("div").addClass("has-error");

                // Style for AdminLTE.
                keyObj.addClass("is-invalid");
            }
        }
    }
}

/**
 * Bootstrap DatePicker helper.
 */
function initDatePicker(datePicker, language, options) {
    if (typeof options === "object") {
        datePicker.datepicker(options);
    } else {
        var opt = defaultDatePickerOptions();

        if (language == "cn") {
            language = "zh-CN";
        }

        opt["language"] = language;

        datePicker.datepicker(opt);
    }
}

/**
 * jQuery DataTables helper.
 */
function initDtSearchButton(searchButton, searchForm, dataTable) {
    searchButton.click(function() {
        if (dataTable != null) {
            dataTable.ajax.reload();
        }
    });

    $("input", searchForm).keyup(function(e) {
        var obj = $(this);

        if (!obj.prop("readonly") && !obj.prop("disabled")) {
            if (e.keyCode == 13) {
                searchButton.trigger("click");
            }
        }
    });
}

/**
 * jQuery DataTables helper.
 */
function initDtExportExcelButton(exportButton, searchForm, dataTable) {
    exportButton.click(function() {
        if (dataTable != null) {
            var data = dataTable.ajax.params();
            var exportClass = "do_export_excel";

            data._act = exportClass;

            for (var key in data) {
                if (typeof data[key] == "object") {
                    searchForm.append(
                        "<input type='hidden' class='" +
                            exportClass +
                            "' name='" +
                            key +
                            "' value='" +
                            JSON.stringify(data[key]) +
                            "'>"
                    );
                } else {
                    searchForm.append(
                        "<input type='hidden' class='" +
                            exportClass +
                            "' name='" +
                            key +
                            "' value='" +
                            data[key] +
                            "'>"
                    );
                }
            }

            searchForm.submit();

            // Immediately remove temp controls after submitting form.
            $("." + exportClass + "", searchForm).remove();
        }
    });
}

/**
 * jQuery DataTables helper.
 */
function getDtSearchParams(searchForm) {
    var obj = {};

    $("input, select", searchForm).each(function() {
        obj[$(this).attr("name")] = $(this).val();
    });

    return JSON.stringify(obj);
}

/**
 * Helper to setup CSRF token.
 */
function initCsrfToken() {
    var csrfMeta = $("meta[name='csrf-token']");

    if (csrfMeta.length) {
        var token = csrfMeta.attr("content");

        if (token.length) {
            $.ajaxSetup({
                headers: {
                    "X-CSRF-TOKEN": token
                }
            });

            $("form").each(function() {
                var form = $(this);

                if (
                    !form.hasClass("no-csrf") &&
                    $("input[name='_token']", form).length < 1
                ) {
                    form.prepend(
                        '<input type="hidden" name="_token" value="' +
                            token +
                            '">'
                    );
                }
            });
        }
    }
}

function initCaptchaImg(captchaObj, refreshObj) {
    captchaObj.click(function() {
        var obj = $(this);
        var config = obj.data("refresh-config");

        $.ajax({
            method: "GET",
            url: "/get_captcha/" + config
        }).done(function(response) {
            obj.prop("src", response);
        });
    });

    if (refreshObj != undefined && refreshObj != null) {
        refreshObj.click(function() {
            captchaObj.click();
        });
    }
}

/**
 * Helper to fetch back inputs from previous session.
 */
function retainInputs(objects) {
    for (var key in objects) {
        if (objects[key].length) {
            // Looking for object by ID.
            var obj = $("#" + key);

            if (obj.length) {
                obj.val(objects[key]);
                continue;
            }

            // ID not found, use NAME.
            obj = $("[name='" + key + "']");

            if (obj.length) {
                if (obj.attr("type") == "radio") {
                    // Special case for radio.
                    $(
                        "input[name='" +
                            key +
                            "'][value='" +
                            objects[key] +
                            "']"
                    ).prop("checked", true);
                    continue;
                }

                obj.val(objects[key]);
                continue;
            }
        }
    }
}

/**
 * Helper to disable all input fields in form.
 */
function disableForm(form, removeSubmit) {
    if (form.length) {
        $("input, select, textarea", form)
            .prop("disabled", true)
            .prop("readonly", true);

        if (removeSubmit == undefined || removeSubmit == true) {
            $("[type='submit']", form).remove();
        }
    }
}

/**
 * Helper to load back inputs from previous session for iChecker.
 */
function retainInputIChecker(objects) {
    for (var key in objects) {
        var objVal = objects[key];

        if (objVal.length) {
            // Looking for object by ID.
            var obj = $("#" + key);

            if (obj.length < 1) {
                // ID not found, use NAME.
                obj = $("[name='" + key + "']");
            }

            if (obj.length) {
                // Object is found.
                if (obj.attr("type") == "radio") {
                    // Special case for radio.
                    $("[name='" + key + "'][value='" + objVal + "']").iCheck(
                        "check"
                    );
                } else if (objVal == obj.val() || objVal == "on") {
                    obj.iCheck("check");
                } else {
                    obj.iCheck("uncheck");
                }
            }
        }
    }
}

/**
 * Helper for integrate template style to jQuery Validator.
 */
function validatorOptWithStyle(opt) {
    if (opt["highlight"] == undefined) {
        opt["highlight"] = function(element) {
            $(element)
                .parent("div")
                .addClass("has-error");

            // Style for AdminLTE.
            $(element).addClass("is-invalid");
        };
    }

    if (opt["unhighlight"] == undefined) {
        opt["unhighlight"] = function(element) {
            $(element)
                .parent("div")
                .removeClass("has-error");

            // Style for AdminLTE.
            $(element).removeClass("is-invalid");
        };
    }

    return opt;
}
